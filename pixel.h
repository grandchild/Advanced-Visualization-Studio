/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_PIXEL
#define AVS_HEADER_PIXEL 1

#include <stdint.h>

#include "blend.h"
#include "common.h"

#if ARCH_X86 /* && CONFIG_ATLEAST_SSE2 */
#include "x86/pixel_intrinsics.h"
#endif

#ifndef blend_pixel_add
#define blend_pixel_add blend_pixel_add_c
#endif

#ifndef blend_pixel_adj
#define blend_pixel_adj blend_pixel_adj_c
#endif

#ifndef blend_pixel_avg
#define blend_pixel_avg blend_pixel_avg_c
#endif

#ifndef blend_pixel_max
#define blend_pixel_max blend_pixel_max_c
#endif

#ifndef blend_pixel_min
#define blend_pixel_min blend_pixel_min_c
#endif

#ifndef blend_pixel_mul
#define blend_pixel_mul blend_pixel_mul_c
#endif

#ifndef blend_pixel_sub
#define blend_pixel_sub blend_pixel_sub_c
#endif

#ifndef blend_pixel_4
#define blend_pixel_4 blend_pixel_4_c
#endif

extern uint8_t blend_table[256][256];
void init_blend_table(void);

static inline
int blend_pixel_add_c(int a, int b)
{
    int ret, temp;
    temp  = (a & 0xff) + (b & 0xff);
    ret   = MIN(temp, 0xff);
    temp  = (a & 0xff00) + (b & 0xff00);
    ret  |= MIN(temp, 0xff00);
    temp  = (a & 0xff0000) + (b & 0xff0000);
    ret  |= MIN(temp, 0xff0000);
    /* TODO: alpha channel. */
    return ret;
}

static inline
int blend_pixel_max_c(int a, int b)
{
    int ret, t1, t2;
    t1 = a & 0xff;
    t2 = b & 0xff;
    ret = MAX(t1, t2);
    t1 = a & 0xff00;
    t2 = b & 0xff00;
    ret |= MAX(t1, t2);
    t1 = a & 0xff0000;
    t2 = b & 0xff0000;
    ret |= MAX(t1, t2);
    return ret;
}

static inline
int blend_pixel_min_c(int a, int b)
{
    int ret, t1, t2;
    t1 = a & 0xff;
    t2 = b & 0xff;
    ret = MIN(t1, t2);
    t1 = a & 0xff00;
    t2 = b & 0xff00;
    ret |= MIN(t1, t2);
    t1 = a & 0xff0000;
    t2 = b & 0xff0000;
    ret |= MIN(t1, t2);
    return ret;
}


static inline
int blend_pixel_avg_c(int a, int b)
{
#if CONFIG_BLEND_PIXEL_ACCURATE_AVG
#define AVG(a,b) (((a) + (b) + 1) / 2)
    return I3_TO_I(AVG(CR_TO_I(a), CR_TO_I(b)),
                   AVG(CG_TO_I(a), CG_TO_I(b)),
                   AVG(CB_TO_I(a), CB_TO_I(b)));
#else
    return ((a>>1)&~((1<<7)|(1<<15)|(1<<23)))+((b>>1)&~((1<<7)|(1<<15)|(1<<23)));
#endif
}

static inline
int blend_pixel_sub_c(int a, int b)
{
    int ret, temp;
    temp  = (a & 0xff) - (b & 0xff);
    ret   = MAX(temp, 0);
    temp  = (a & 0xff00) - (b & 0xff00);
    ret  |= MAX(temp, 0);
    temp  = (a & 0xff0000) - (b & 0xff0000);
    ret  |= MAX(temp, 0);
    /* TODO: alpha channel. */
    return ret;
}

static inline
int blend_pixel_mul_c(int a, int b)
{
#if CONFIG_BLEND_PIXEL_ACCURATE_MUL
    return I3_TO_I(blend_table[CR_TO_I(a)][CR_TO_I(b)],
                   blend_table[CG_TO_I(a)][CG_TO_I(b)],
                   blend_table[CB_TO_I(a)][CB_TO_I(b)]);
#else
    return I3_TO_I((CR_TO_I(a) * CR_TO_I(b)) / 256,
                   (CG_TO_I(a) * CG_TO_I(b)) / 256,
                   (CB_TO_I(a) * CB_TO_I(b)) / 256);
#endif
}

static inline
int blend_pixel_adj_c(int a, int b, int v)
{
#if CONFIG_BLEND_PIXEL_ACCURATE_MUL
    return I3_TO_I(blend_table[CR_TO_I(a)][v] + blend_table[CR_TO_I(b)][0xff - v],
                   blend_table[CG_TO_I(a)][v] + blend_table[CG_TO_I(b)][0xff - v],
                   blend_table[CB_TO_I(a)][v] + blend_table[CB_TO_I(b)][0xff - v]);
#else
    return I3_TO_I((CR_TO_I(a) * v + CR_TO_I(b) * (255 - v)) / 256,
                   (CG_TO_I(a) * v + CG_TO_I(b) * (255 - v)) / 256,
                   (CB_TO_I(a) * v + CB_TO_I(b) * (255 - v)) / 256);
#endif
}

static inline
void blend_pixel(int *pixel, int colour, int blend_mode)
{
    switch (blend_mode_get_mode(blend_mode)) {
        case BM_ADDITIVE:
            *pixel = blend_pixel_add(*pixel, colour);
            break;
        case BM_MAXIMUM:
            *pixel = blend_pixel_max(*pixel, colour);
            break;
        case BM_AVERAGE:
            *pixel = blend_pixel_avg(*pixel, colour);
            break;
        case BM_SUBTRACTIVE1:
            *pixel = blend_pixel_sub(*pixel, colour);
            break;
        case BM_SUBTRACTIVE2:
            *pixel = blend_pixel_sub(colour, *pixel);
            break;
        case BM_MULTIPLY:
            *pixel = blend_pixel_mul(*pixel, colour);
            break;
        case BM_ADJUSTABLE:
            *pixel = blend_pixel_adj(*pixel, colour, blend_mode_get_adj(blend_mode));
            break;
        case BM_XOR:
            *pixel = *pixel ^ colour;
            break;
        case BM_MINIMUM:
            *pixel = blend_pixel_min(*pixel, colour);
            break;
        default:
            *pixel = colour;
    }
}

static inline
int blend_pixel_4_c(int *p, int w, int xp, int yp)
{
    uint8_t a1 = blend_table[255 - xp][255 - yp];
    uint8_t a2 = blend_table[xp][255 - yp];
    uint8_t a3 = blend_table[255 - xp][yp];
    uint8_t a4 = blend_table[xp][yp];
    return I3_TO_I(blend_table[CR_TO_I(p[0])][a1]
                 + blend_table[CR_TO_I(p[1])][a2]
                 + blend_table[CR_TO_I(p[w])][a3]
                 + blend_table[CR_TO_I(p[w + 1])][a4],
                   blend_table[CG_TO_I(p[0])][a1]
                 + blend_table[CG_TO_I(p[1])][a2]
                 + blend_table[CG_TO_I(p[w])][a3]
                 + blend_table[CG_TO_I(p[w + 1])][a4],
                   blend_table[CB_TO_I(p[0])][a1]
                 + blend_table[CB_TO_I(p[1])][a2]
                 + blend_table[CB_TO_I(p[w])][a3]
                 + blend_table[CB_TO_I(p[w + 1])][a4]);
}

static inline
int blend_pixel_4_16(int *p, int w, int xp, int yp)
{
    xp = (xp >> 8) & 0xff;
    yp = (xp >> 8) & 0xff;
    return blend_pixel_4(p, w, xp, yp);
}

#endif /* AVS_HEADER_PIXEL */
