/*
 * Copyright (c) 2015 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fenv.h>
#include "expressions.h"

#if 0
#include "timer.h"
#else
#define START_TIMER
#define STOP_TIMER(a)
#endif

#define FUNC(OP, MSG) \
static int blend_pixel_op_ ## OP(lua_State *L) \
{ \
    int a = lua_tointeger(L, 1); \
    int b = lua_tointeger(L, 2); \
    START_TIMER \
    int ret = blend_pixel_ ## OP(a,b); \
    STOP_TIMER(MSG) \
    lua_pushinteger(L, ret); \
    return 1; \
}

#include "pixel.h"
#if ARCH_X86
#include "x86/pixel_intrinsics.h"
#endif

FUNC(add_c, "add_c")
FUNC(avg_c, "avg_c")
FUNC(max_c, "max_c")
FUNC(min_c, "min_c")
FUNC(mul_c, "mul_c")
FUNC(sub_c, "sub_c")

FUNC(add_intrinsic, "add_i")
FUNC(avg_intrinsic, "avg_i")
FUNC(max_intrinsic, "max_i")
FUNC(min_intrinsic, "min_i")
FUNC(mul_intrinsic, "mul_i")
FUNC(sub_intrinsic, "sub_i")

static int blend_pixel_op_adj_c(lua_State *L)
{
    int a = lua_tointeger(L, 1);
    int b = lua_tointeger(L, 2);
    int c = lua_tointeger(L, 3);
    START_TIMER
    int ret = blend_pixel_adj_c(a, b, c);
    STOP_TIMER("adj_c")
    lua_pushinteger(L, ret);
    return 1;
}

static int blend_pixel_op_adj_intrinsic(lua_State *L)
{
    int a = lua_tointeger(L, 1);
    int b = lua_tointeger(L, 2);
    int c = lua_tointeger(L, 3);
    START_TIMER
    int ret = blend_pixel_adj_intrinsic(a, b, c);
    STOP_TIMER("adj_i")
    lua_pushinteger(L, ret);
    return 1;
}

static int blend_pixel_op_pix4_c(lua_State *L)
{
    int pixel[4] = {
        lua_tointeger(L, 1),
        lua_tointeger(L, 2),
        lua_tointeger(L, 3),
        lua_tointeger(L, 4),
    };
    int xp = lua_tointeger(L, 5);
    int yp = lua_tointeger(L, 6);
    START_TIMER
    int ret = blend_pixel_4_c(pixel, 2, xp, yp);
    STOP_TIMER("pix4_c")
    lua_pushinteger(L, ret);
    return 1;
}

static int blend_pixel_op_pix4_intrinsic(lua_State *L)
{
    int pixel[4] = {
        lua_tointeger(L, 1),
        lua_tointeger(L, 2),
        lua_tointeger(L, 3),
        lua_tointeger(L, 4),
    };
    int xp = lua_tointeger(L, 5);
    int yp = lua_tointeger(L, 6);
    START_TIMER
    int ret = blend_pixel_4_intrinsic(pixel, 2, xp, yp);
    STOP_TIMER("pix4_i")
    lua_pushinteger(L, ret);
    return 1;
}

#define REG(OP) \
    { #OP, blend_pixel_op_ ## OP ## _c, blend_pixel_op_ ## OP ## _intrinsic }

static const struct {
    const char *op;
    int (*c)(lua_State *L);
    int (*i)(lua_State *L);
} functions[] = {
    REG(add),
    REG(avg),
    REG(max),
    REG(min),
    REG(mul),
    REG(sub),
    REG(adj),
    REG(pix4),
};

int main(void)
{
    init_blend_table();

    lua_State *L = luaL_newstate();
    if (!L)
        return 1;
    luaL_openlibs(L);

    lua_createtable(L, lengthof(functions), 0);
    for (int i = 0; i < lengthof(functions); i++) {
        lua_pushinteger(L, i+1);
        lua_createtable(L, 0, 3);
        lua_pushstring(L, "c");
        lua_pushcfunction(L, functions[i].c);
        lua_settable(L, -3);
        lua_pushstring(L, "i");
        lua_pushcfunction(L, functions[i].i);
        lua_settable(L, -3);
        lua_pushstring(L, "op");
        lua_pushstring(L, functions[i].op);
        lua_settable(L, -3);
        lua_settable(L, -3);
    }
    lua_setglobal(L, "pixel_ops");

    int ret = luaL_dofile(L, "tests/pixel.lua");
    if (ret) {
        printf("%s\n", lua_tostring(L, 1));
    }
    lua_close(L);

    return ret;
}
