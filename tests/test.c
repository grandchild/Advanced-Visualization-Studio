/*
 * Copyright (c) 2014-2015 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <inttypes.h>
#include <getopt.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#if SYS_WINDOWS
#include <windows.h>
#undef ERROR /* Windows defines this somewhere so this silences a warning. */
#endif

#include "avs.h"
#include "common.h"

enum  {
    OPT_FILE_INPUT = 'i',
    OPT_FILE_OUTPUT = 'o',
    OPT_PRINT = 'p',
    OPT_SIZE = 's',
    OPT_VERBOSE = 'v',
};

struct TestOptions {
    char *file_input;
    char *file_output;
    int width, height;
    int print;
    int verbose;
};

static void default_options(struct TestOptions *opt)
{
    memset(opt, 0, sizeof(struct TestOptions));
    opt->width = 320;
    opt->height = 240;
    opt->verbose = AVS_LOG_INFO;
}

static int parse_opts(int argc, char ** argv, struct TestOptions *options)
{
    struct option cmd_options[] = {
        { "input",   required_argument, 0, OPT_FILE_INPUT },
        { "output",  required_argument, 0, OPT_FILE_OUTPUT },
        { "print",         no_argument, 0, OPT_PRINT },
        { "size",    required_argument, 0, OPT_SIZE },
        { "verbose",       no_argument, 0, OPT_VERBOSE },
        { 0 }
    };

    while (1) {
        int option_index;
        int arg = getopt_long(argc, argv, "i:o:ps:v", cmd_options, &option_index);

        if (arg == -1)
            break;

        switch (arg) {
            case OPT_FILE_INPUT: {
                options->file_input = optarg;
            } break;

            case OPT_FILE_OUTPUT: {
                options->file_output = optarg;
            } break;

            case OPT_PRINT: {
                options->print = 1;
            } break;

            case OPT_SIZE: {
                const char *sub;
                int w = 0, h = 0;

                w = strtol(optarg, (void*)&sub, 10);
                if (*sub)
                    sub++;
                h = strtol(sub, (void*)&sub, 10);

                if (w <= 0 || h <= 0) {
                    ERROR("Invalid size given: %s\n", optarg);
                    return -1;
                }

                options->width = w;
                options->height = h;
            } break;

            case OPT_VERBOSE: {
                options->verbose++;
            } break;

            default:
                //fprintf(stderr, "Error: Unknown option: %s\n", argv[option_index]);
                return -1;
        }
    }

    if (!options->file_input) {
        ERROR("No input file given\n");
        return -1;
    }

    return 0;
}

static void sine_wave(uint8_t data[576], int amplitude, int periods)
{
    for (int i = 0; i < 576; i++)
        data[i] = sin((i * periods * 2 * M_PI)/575.0) * amplitude + 128.0;
}

static int save_raw_frame(AVSContext *actx, int w, int h, const char *filename)
{
    FILE *fh = NULL;

    fh = fopen((const char *)filename, "wb");
    if (!fh)
        goto error;

    int* frame = avs_get_frame(actx);
    for (int p = 0; p < w * h; ++p) {
        int pixel =
            ((frame[p] & 0x0000ff) << 16) | /* invert BGR to RGB */
            (frame[p] & 0x00ff00) |
            ((frame[p] & 0xff0000) >> 16) |
            0xff000000;                     /* and add alpha channel */
        fwrite(&pixel, sizeof(int), 1, fh);
    }

    fclose(fh);
    return 0;

error:
    if (fh)
        fclose(fh);

    return -1;
}

int main(int argc, char **argv)
{
    struct TestOptions options;
    uint8_t waveform[576], spectrum[576];

    AVSContext *actx = 0;
    AVSDataContext avsdc = {
        .waveform = { waveform, waveform, waveform },
        .spectrum = { spectrum, spectrum, spectrum },
    };
    AVSOptionsContext avs_options = {
        .structure_size = sizeof(avs_options),
        .api_version = AVS_API_VERSION,
    };

    int ret = 0;

    default_options(&options);

    ret = parse_opts(argc, argv, &options);
    if (ret)
        goto error;

    avs_options.width = options.width;
    avs_options.height = options.height;
    avs_options.log_level = options.verbose;

    ret = avs_init(&actx, &avs_options) || avs_load_preset(actx, options.file_input);
    if (ret)
        goto error;

    if (options.print)
        avs_print_preset(actx);

    memset(spectrum,      51, 128);
    memset(spectrum+128, 102, 128);
    memset(spectrum+256, 154, 128);
    memset(spectrum+384, 204, 128);
    sine_wave(waveform, 96, 1);

    ret = avs_render_frame(actx, &avsdc, 1);
    if (ret)
        goto error;

    if (options.file_output) {
        ret = save_raw_frame(actx, options.width, options.height, options.file_output);
        if (ret)
            goto error;
    }

error:
    avs_uninit(actx);

    return ret;
}
