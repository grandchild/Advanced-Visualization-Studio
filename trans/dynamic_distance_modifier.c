/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "expressions.h"
#include "pixel.h"

typedef struct {
    const char *expr_point;
    const char *expr_beat;
    const char *expr_frame;
    const char *expr_init;
    void *expr_context;
    int blend, subpixel;
    int m_wt, m_lastw, m_lasth;
    int *m_wmul;
    int *m_tab;
    int inited;
    double d_max;
    int effect;
} DDMContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    DDMContext *ddm = ctx->priv;
    void *expr_ctx;
    int pos = 0;
    int temp;

    ddm->expr_context = expr_ctx = expr_init(ctx->actx);
    if (!expr_ctx)
        return -1;

    if (*buf == 1) {
        int ret;
        pos++;

        ret = expr_load_rstring(expr_ctx, &ddm->expr_point, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &ddm->expr_frame, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &ddm->expr_beat, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &ddm->expr_init, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;
    } else {
        /* read fixed len */
            ERROR("fixed-length expressions not yet implemented\n");
    }

    R32(temp); /* blend */
    ddm->effect |= temp ? 1 : 0;
    R32(temp); /* subpixel */
    ddm->effect |= temp ? 2 : 0;

    if (expr_compile(expr_ctx, "__avs_internal_init", ddm->expr_init)
            || expr_compile(expr_ctx, "__avs_internal_frame", ddm->expr_frame)
            || expr_compile(expr_ctx, "__avs_internal_beat", ddm->expr_beat)
            || expr_compile(expr_ctx, "__avs_internal_point", ddm->expr_point))
        return -1;


    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    DDMContext *ddm = ctx->priv;
    double d_max = sqrt((w*w + h*h) / 4.0);
    int i_max = MAX(d_max + 33, 33);

    if (ddm->m_lasth != h || ddm->m_lastw != w || !ddm->m_tab) {
        if (ddm->m_tab)
            free(ddm->m_tab);
        ddm->m_tab = malloc(i_max * sizeof(int));
        if (!ddm->m_tab)
            return -1;

        ddm->m_lastw = w;
        ddm->m_lasth = h;
    }

#define SET(a,b) expr_variable_set(ddm->expr_context, (a), (b))
#define GET(a) expr_variable_get(ddm->expr_context, (a))

    SET("b", is_beat ? 1.0 : 0.0);

    if (!ddm->inited) {
        if (expr_execute(ddm->expr_context, "__avs_internal_init", ddm->expr_init))
            return -1;
        ddm->inited = 1;
    }

    if (expr_execute(ddm->expr_context, "__avs_internal_frame", ddm->expr_frame))
        return -1;

    if (is_beat)
        if (expr_execute(ddm->expr_context, "__avs_internal_beat", ddm->expr_beat))
            return -1;

    if (ddm->expr_point) {
        for (int i = 0; i < i_max; i++) {
            SET("d", i / (d_max - 1.0));
            if (expr_execute(ddm->expr_context, "__avs_internal_point", ddm->expr_point))
                return -1;
            ddm->m_tab[i] = GET("d") * 256.0 * d_max / (i+1);
        }
        for (int i = i_max - 32; i < i_max; i++)
            ddm->m_tab[i] = ddm->m_tab[i-1];
    } else
        memset(ddm->m_tab, 0, ddm->d_max + 33);

    switch (ddm->effect) {
        case 3: /* subpixel && blend */
            for (int y = 0; y < h; y++) {
                int ty = y - h/2;
                int x2 = (w/2 * w/2) + w/2 + ty * ty + 256;
                int dx2 = -w;
                int yysc = ty;
                int xxsc = -w/2;

                for (int x = 0; x < w; x++) {
                    int qd = ddm->m_tab[(int)sqrt(x2)];
                    int x_part = qd * xxsc + 128;
                    int y_part = qd * yysc + 128;
                    int ow = CLIP(w/2 + (x_part >> 8), 0, w-2);
                    int oh = CLIP(h/2 + (y_part >> 8), 0, h-2);

                    x_part &= 0xff;
                    y_part &= 0xff;
                    xxsc++;
                    x2 += dx2;
                    dx2 += 2;

                    frame_out[y*w+x] = blend_pixel_avg(blend_pixel_4(frame + ow + oh * w, w, x_part, y_part), frame[y*w+h]);
                }
            }
            break;

        case 2: /* subpixel */
            for (int y = 0; y < h; y++) {
                int ty = y - h/2;
                int x2 = (w/2 * w/2) + w/2 + ty * ty + 256;
                int dx2 = -w;
                int yysc = ty;
                int xxsc = -w/2;

                for (int x = 0; x < w; x++) {
                    int qd = ddm->m_tab[(int)sqrt(x2)];
                    int x_part = qd * xxsc + 128;
                    int y_part = qd * yysc + 128;
                    int ow = CLIP(w/2 + (x_part >> 8), 0, w-2);
                    int oh = CLIP(h/2 + (y_part >> 8), 0, h-2);

                    x_part &= 0xff;
                    y_part &= 0xff;
                    xxsc++;
                    x2 += dx2;
                    dx2 += 2;

                    frame_out[y*w+x] = blend_pixel_4(frame + ow + oh * w, w, x_part, y_part);
                }
            }
            break;

        case 1: /* blend */
            for (int y = 0; y < h; y++) {
                int ty = y - h/2;
                int x2 = (w/2 * w/2) + w/2 + ty * ty + 256;
                int dx2 = -w;
                int yysc = ty;
                int xxsc = -w/2;

                for (int x = 0; x < w; x++) {
                    int qd = ddm->m_tab[(int)sqrt(x2)];
                    int ow = CLIP(w/2 + ((qd * xxsc + 128) >> 8), 0, w-1);
                    int oh = CLIP(h/2 + ((qd * yysc + 128) >> 8), 0, h-1);

                    xxsc++;
                    x2 += dx2;
                    dx2 += 2;

                    frame_out[y*w+x] = blend_pixel_avg(frame[ow + oh * w], frame[y*w+h]);
                }
            }
            break;

        default:
            for (int y = 0; y < h; y++) {
                int ty = y - h/2;
                int x2 = (w/2 * w/2) + w/2 + ty * ty + 256;
                int dx2 = -w;
                int yysc = ty;
                int xxsc = -w/2;

                for (int x = 0; x < w; x++) {
                    int qd = ddm->m_tab[(int)sqrt(x2)];
                    int ow = CLIP(w/2 + ((qd * xxsc + 128) >> 8), 0, w-1);
                    int oh = CLIP(h/2 + ((qd * yysc + 128) >> 8), 0, h-1);

                    xxsc++;
                    x2 += dx2;
                    dx2 += 2;

                    frame_out[y*w+x] = frame[ow + oh * w];
                }
            }
            break;
    }

    return 1;
}

static void uninit(ComponentContext *ctx)
{
    DDMContext *ddm = ctx->priv;

    FREE((void *)ddm->expr_init);
    FREE((void *)ddm->expr_frame);
    FREE((void *)ddm->expr_beat);
    FREE((void *)ddm->expr_point);
    FREE(ddm->m_tab);

    expr_uninit(ddm->expr_context);
}

Component t_ddm = {
    .name = "Trans / Dynamic Distance Modifier",
    .code = 35,
    .priv_size = sizeof(DDMContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
