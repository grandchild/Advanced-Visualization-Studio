/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "block.h"
#include "components.h"
#include "common.h"
#include "expressions.h"
#include "pixel.h"

typedef struct {
    int blend;
    int subpixel;
    const char *expr_init;
    const char *expr_frame;
    const char *expr_beat;
    void *expr_context;
    int inited;
    int last_w, last_h;
} DynamicShiftContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    DynamicShiftContext *dyn = ctx->priv;
    int pos = 0;

    void *expr_ctx = expr_init(ctx->actx);
    if (!expr_ctx)
        return -1;

    if (*buf == 1) {
        int ret = 0;
        pos++;

        ret = expr_load_rstring(expr_ctx, &dyn->expr_init, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &dyn->expr_frame, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &dyn->expr_beat, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;
    } else {
        /* read fixed len */
            ERROR("fixed-length expressions not yet implemented\n");
    }

    R32(dyn->blend);
    R32(dyn->subpixel);

    if (expr_compile(expr_ctx, "__avs_internal_init", dyn->expr_init)
            || expr_compile(expr_ctx, "__avs_internal_frame", dyn->expr_frame)
            || expr_compile(expr_ctx, "__avs_internal_beat", dyn->expr_beat))
        return -1;

    dyn->expr_context = expr_ctx;

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    DynamicShiftContext *dyn = ctx->priv;

#define SET(a,b) expr_variable_set(dyn->expr_context, (a), (b))
#define GET(a) expr_variable_get(dyn->expr_context, (a))

    SET("w", w);
    SET("h", h);
    SET("b", is_beat ? 1.0 : 0.0);

    if (!dyn->inited || dyn->last_w != w || dyn->last_h != h) {
        SET("alpha", 0.5); /* Only set the default value once. */
        if(expr_execute(dyn->expr_context, "__avs_internal_init", dyn->expr_init))
            return -1;
        dyn->inited = 1;
    }
    if (expr_execute(dyn->expr_context, "__avs_internal_frame", dyn->expr_frame))
        return -1;

    if (is_beat)
        if (expr_execute(dyn->expr_context, "__avs_internal_beat", dyn->expr_beat))
            return -1;

    int do_blend = dyn->blend;
    int alpha = 127;

    if (do_blend) {
        alpha = GET("alpha") / 255.0;

        if (alpha <= 0)
            return 0;

        if (alpha >= 255)
            do_blend = 0;
    }

    if (dyn->subpixel) {
        double x_var = GET("x");
        double y_var = GET("y");
        int x = x_var;
        int y = y_var;
        int x_part = (x_var - (int)(x)) * 255.0;
        int y_part = (y_var - (int)(y)) * 255.0;

        if (x_part < 0)
            x_part = -x_part;
        else {
            x++;
            x_part = 255 - x_part;
        }
        x_part = CLIP(x_part, 0, 255);

        if (y_part < 0)
            y_part = -y_part;
        else {
            y++;
            y_part = 255 - y_part;
        }
        y_part = CLIP(y_part, 0, 255);

        x = CLIP(x, 1-w, w-1);
        y = CLIP(y, 1-h, h-1);

        int x_end = CLIP(w-1+x, 0, w-1);
        int y_end = CLIP(h-1+y, 0, h-1);

        int row;

        for (row = 0; row < y; row++)
            memset(frame_out + row * w, 0, w * sizeof(int));

        for (; row < y_end; row++) {
            int col;

            for (col = 0; col < x; col++)
                frame_out[row * w + col] = 0;

            for (; col < x_end; col++)
                frame_out[row * w + col] = blend_pixel_4(frame + (row - y) * w + (col - x), w, x_part, y_part);

            for (; col < w; col++)
                frame_out[row * w + col] = 0;
        }

        for (; row < y; row++)
            memset(frame_out + row * w, 0, w * sizeof(int));
        /* End of subpixel. */
    } else {
        int x = GET("x");
        int y = GET("y");

        int x_end = MIN(w+x, w);
        int y_end = MIN(h+y, h);

        x = MIN(x, w);
        y = MIN(y, h);

        int row;

        for (row = 0; row < y; row++)
            memset(frame_out + row * w, 0, w * sizeof(int));

        for (; row < y_end; row++) {
            int col;

            for (col = 0; col < x; col++)
                frame_out[row * w + col] = 0;

            for (; col < x_end; col++)
                frame_out[row * w + col] = frame[(row - y) * w + (col - x)];

            for (; col < w; col++)
                frame_out[row * w + col] = 0;
        }

        for (; row < y; row++)
            memset(frame_out + row * w, 0, w * sizeof(int));
    }

    if (do_blend)
        blend_block_adj(frame_out, frame_out, frame, w*h, alpha);

    return 1;
}

static void uninit(ComponentContext *ctx)
{
    DynamicShiftContext *dyn = ctx->priv;

    FREE((void *)dyn->expr_init);
    FREE((void *)dyn->expr_frame);
    FREE((void *)dyn->expr_beat);

    expr_uninit(dyn->expr_context);
}

Component t_dynamicshift = {
    .name = "Trans / Dynamic Shift",
    .code = 42,
    .priv_size = sizeof(DynamicShiftContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
