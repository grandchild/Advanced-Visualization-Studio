/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "block.h"
#include "components.h"
#include "common.h"
#include "expressions.h"
#include "pixel.h"

enum new_effect_modes {
    N_EFFECT_SOURCE_MAP_ON = 1,
    N_EFFECT_SOURCE_MAP_BEAT = 2,
    N_EFFECT_WRAP = 4,
    N_EFFECT_BLEND_INPUT = 8,
    N_EFFECT_BILINEAR = 16,
};

typedef struct {
    int *trans_tab;
    int trans_tab_w, trans_tab_h, trans_tab_subpixel;
    int trans_effect;
    const char *effect_expr;
    int effect_exp_ch;
    int effect;
    int blend;
    int sourcemapped;
    int rectangular;
    int subpixel;
    int wrap;
    void *expr_context;
    int new_effect;
} MovementContext;

#define REFFECT_MIN 3
#define REFFECT_MAX 23

#define MAKE_REFFECT(n, x) \
    static void ref##n(unused float *r, unused float *d, unused float max_d, unused int *xo, unused int *yo) { x }

MAKE_REFFECT( 3, *r += 0.1 - 0.2 * (*d / max_d); *d *= 0.96;)
MAKE_REFFECT( 4, *d *= 0.99 * (1.0 - sinf(*r) / 32.0); *r += 0.03 * sinf(*d / max_d * M_PI * 4);)
MAKE_REFFECT( 5, *d *= 0.94 + (cosf(*r * 32.0) * 0.06);)
MAKE_REFFECT( 6, *d *= 1.01 + (cosf(*r * 4.0) * 0.04); *r += 0.03 * sinf(*d / max_d * M_PI * 4);)
MAKE_REFFECT( 8, *r += 0.1 * sinf(*d / max_d * M_PI * 5);)
MAKE_REFFECT( 9, float t = sinf(*d / max_d * M_PI); *d -= 8 * t*t*t*t*t; )
MAKE_REFFECT(10, float t = sinf(*d  /max_d * M_PI); *d -= 8 * t*t*t*t*t; t = cosf((*d / max_d) * M_PI / 2.0); *r += 0.1 * t*t*t; )
MAKE_REFFECT(11, *d *= 0.95 + (cosf(*r * 5.0 - M_PI / 2.50) * 0.03); )
MAKE_REFFECT(12, *r += 0.04; *d *= 0.96 + cosf(*d / max_d * M_PI) * 0.05; )
MAKE_REFFECT(13, float t = cosf(*d / max_d*M_PI); *r += 0.07 * t; *d *= 0.98 + t * 0.10; )
MAKE_REFFECT(14, *r += 0.1 - 0.2 * (*d / max_d); *d *= 0.96; *xo = 8; )
MAKE_REFFECT(15, *d = max_d * 0.15;)
MAKE_REFFECT(16, *r = cosf(*r * 3);)
MAKE_REFFECT(17, *d *= (1 - (((*d / max_d) - 0.35) * 0.5)); *r += 0.1;)

struct {
    const char *name;
    const char *expr;
    int uses_expr;
    int uses_rect;
    void (*func)(float *r, float *d, float max_d, int *xo, int *yo);
} descriptions[] = {
    {
        .name = "none",
    },
    {
        .name = "slight fuzzify",
    },
    {
        .name = "shift rotate left",
        .expr = "x = x + 1 / 32;",
        .uses_rect = 1,
    },
    {
        .name = "big swirl out",
        .expr = "r = r + (0.1 - (0.2 * d));"
                "d = d * 0.96;",
        .func = ref3,
    },
    {
        .name = "medium swirl",
        .expr = "d = d * (0.99 * (1.0 - math.sin(r - math.pi * 0.5) / 32.0));"
                "r = r + (0.03 * math.sin(d * math.pi * 4));",
        .func = ref4,
    },
    {
        .name = "sunburster",
        .expr = "d = d * (0.94 + (math.cos((r - math.pi * 0.5) * 32.0) * 0.06));",
        .func = ref5,
    },
    {
        .name = "swirl to center",
        .expr = "d = d * (1.01 + (math.cos((r - math.pi * 0.5) * 4) * 0.04));"
                "r = r + (0.03 * math.sin(d * math.pi * 4));",
        .func = ref6,
    },
    {
        .name = "blocky partial out",
    },
    {
        .name = "swirling around both ways at once",
        .expr = "r = r + (0.1 * math.sin(d * math.pi * 5));",
        .func = ref8,
    },
    {
        .name = "bubbling outward",
        .expr = "t = math.sin(d * math.pi);"
                "d = d - (8 * t*t*t*t*t) / math.sqrt((sw * sw + sh * sh) / 4);",
        .func = ref9,
    },
    {
        .name = "bubbling outward with swirl",
        .expr = "t = math.sin(d * math.pi);"
                "d = d - (8 * t*t*t*t*t) / math.sqrt((sw * sw + sh * sh) / 4);"
                "t = math.cos(d * math.pi / 2.0);"
                "r = r + 0.1 * t*t*t;",
        .func = ref10,
    },
    {
        .name = "5 pointed distro",
        .expr = "d = d * (0.95 + (math.cos(((r - math.pi * 0.5) * 5.0) - (math.pi / 2.50)) * 0.03));",
        .func = ref11,
    },
    {
        .name = "tunneling",
        .expr = "r = r + 0.04;"
                "d = d * (0.96 + math.cos(d * math.pi) * 0.05);",
        .func = ref12,
    },
    {
        .name = "bleedin'",
        .expr = "t = math.cos(d * math.pi);"
                "r = r + (0.07 * t);"
                "d = d * (0.98 + t * 0.10);",
        .func = ref13,
    },
    {
        .name = "shifted big swirl out",
        .expr = // FIXME: this is a very bad approximation in script.
                "d=sqrt(x*x+y*y);"
                "r=atan2(y,x);"
                "r=r+0.1-0.2*d;"
                "d=d*0.96;"
                "x=cos(r)*d + 8/128;"
                "y=sin(r)*d;",
        .uses_rect = 1,
        .func = ref14,
    },
    {
        .name = "psychotic beaming outward",
        .expr = "d = 0.15;",
        .func = ref15,
    },
    {
        .name = "cosine radial 3-way",
        .expr = "r = math.cos(r * 3);",
        .func = ref16,
    },
    {
        .name = "spinny tube",
        .expr = "d = d * (1 - ((d - 0.35) * 0.5));"
                "r = r + 0.1;",
        .func = ref17,
    },
    {
        .name = "radial swirlies",
        .expr = "d = d * (1 - (math.sin((r - math.pi * 0.5) * 7) * 0.03));"
                "r = r + (math.cos(d * 12) * 0.03);",
        .uses_expr = 1,
    },
    {
        .name = "swill",
        .expr = "d = d * (1 - (math.sin((r - math.pi * 0.5) * 12) * 0.05));"
                "r = r + (math.cos(d * 18) * 0.05);"
                "d = d * (1 - ((d - 0.4) * 0.03));"
                "r = r + ((d - 0.4) * 0.13);",
        .uses_expr = 1,
    },
    {
        .name = "gridley",
        .expr = "x = x + (math.cos(y * 18) * 0.02);"
                "y = y + (math.sin(x * 14) * 0.03);",
        .uses_expr = 1,
        .uses_rect = 1,
    },
    {
        .name = "grapevine",
        .expr = "x = x + (math.cos(math.abs(y - 0.5) * 8) * 0.02);"
                "y = y + (math.sin(math.abs(x - 0.5) * 8) * 0.05);"
                "x = x * 0.95;"
                "y = y * 0.95;",
        .uses_expr = 1,
        .uses_rect = 1,
    },
    {
        .name = "quadrant",
        .expr = "y = y * (1 + (math.sin(r + math.pi / 2) * 0.3));"
                "x = x * (1 + (math.cos(r + math.pi / 2) * 0.3));"
                "x = x * 0.995;"
                "y = y * 0.995;",
        .uses_expr = 1,
        .uses_rect = 1,
    },
    {
        .name = "6-way kaleida (use wrap!)",
        .expr = "y = (r * 6) / (math.pi);"
                "x = d;",
        .uses_expr = 1,
        .uses_rect = 1,
    },
};

static int effect_uses_eval(int t)
{
    int ret = 0;
    if (t >= REFFECT_MIN && t <= REFFECT_MAX)
        if (descriptions[t].uses_expr)
            ret = 1;
    return ret;
}

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    MovementContext *mctx = ctx->priv;
    int pos = 0;
    int effect, ret;

    mctx->expr_context = expr_init(ctx->actx);
    if (!mctx->expr_context)
        return -1;

    R32(effect);

    if (effect == 32767) {
        if (!memcmp(buf + pos, "!rect ", 6)) {
            pos += 6;
            mctx->rectangular = 1;
        }

        if (buf[pos] == 1) {
            pos++;
            ret = expr_load_rstring(mctx->expr_context, &mctx->effect_expr, buf + pos, buf_len - pos);
            if (ret < 0)
                return -1;
            pos += ret;
        } else if (buf_len - pos >= 256) {
            /* read fixed len */
            ERROR("fixed-length expressions not yet implemented\n");
        }
    }
    R32(mctx->blend);
    R32(mctx->sourcemapped);
    R32(mctx->rectangular);
    R32(mctx->subpixel);
    R32(mctx->wrap);
    if (!effect)
        R32(effect);

    if ((effect != 32767 && effect > REFFECT_MAX) || effect < 0)
        effect = 0;

    mctx->effect = effect;
    mctx->effect_exp_ch = 1;

    mctx->new_effect  = mctx->sourcemapped & 2 ? N_EFFECT_SOURCE_MAP_BEAT : 0;
    mctx->new_effect |= mctx->sourcemapped & 1 ? N_EFFECT_SOURCE_MAP_ON   : 0;
    mctx->new_effect |= mctx->wrap             ? N_EFFECT_WRAP            : 0;
    mctx->new_effect |= mctx->blend            ? N_EFFECT_BLEND_INPUT     : 0;
    mctx->new_effect |= mctx->subpixel         ? N_EFFECT_BILINEAR        : 0;

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    MovementContext *mctx = ctx->priv;

    /* start of smp_begin */

    if (!mctx->effect)
        return 0;

    /* TODO: should this be some separate "init" function? */
    if (!mctx->trans_tab || mctx->trans_tab_w != w || mctx->trans_tab_h != h
            || mctx->effect != mctx->trans_effect) {
        int *transp = malloc(w * h * sizeof(int));
        if (!transp)
            return -1;
        mctx->trans_tab = transp;
        mctx->trans_tab_w = w;
        mctx->trans_tab_h = h;
        mctx->trans_effect = mctx->effect;

        /* WTF is this mess? */
        mctx->trans_tab_subpixel = mctx->subpixel && w * h < (1 << 22)
            && ((mctx->effect >= REFFECT_MIN && mctx->effect <= REFFECT_MAX
                    && mctx->effect != 1 && mctx->effect != 2
                    && mctx->effect != 7) || mctx->effect == 32767);

        int p = 0;

        if (mctx->effect == 1)
            for (int x = w * h; x; x--) {
                int r = (p++) + (rand() % 3) - 1 + ((rand() % 3) - 1) * w;
                *transp++ = MIN(w * h, MAX(r, 0));
            }
        else if (mctx->effect == 2)
            for (int y = h; y; y--) {
                int lp = w / 64;
                for (int x = w; x; x--) {
                    *transp++ = p + lp++;
                    if (lp >= w)
                        lp -= w;
                }
                p += w;
            }
        else if (mctx->effect == 7)
            for (int y = 0; y < h; y++)
                for (int x = 0; x < w; x++)
                    if (x&2 || y&2)
                        *transp++ = x + y * w;
                    else {
                        int xp = w/2 + (((x&~1) - w/2) * 7) / 8;
                        int yp = h/2 + (((y&~1) - h/2) * 7) / 8;
                        *transp++ = xp + yp * w;
                    }
        else if (mctx->effect >= REFFECT_MIN && mctx->effect <= REFFECT_MAX
                && !effect_uses_eval(mctx->effect)) {
            float max_d = sqrtf((w * w + h * h) / 4.0);
            void (*func)(float *r, float *d, float max_d, int *xo, int *yo);

            func = descriptions[mctx->effect].func;

            if (!func) {
                ERROR("no compiled function available for effect = %d\n", mctx->effect);
                return -1;
            }

            for (int y = 0; y < h; y++)
                for (int x = 0; x < w; x++) {
                    float xd = x - (w/2);
                    float yd = y - (h/2);
                    float d = sqrtf(xd * xd  + yd * yd);
                    float r = atan2f(yd, xd);
                    int xo = 0;
                    int yo = 0;

                    func(&r, &d, max_d, &xo, &yo);

                    float tmp1 = h/2 + sinf(r) * d + 0.5 + yo * h * (1.0/256.0);
                    float tmp2 = w/2 + cosf(r) * d + 0.5 + xo * w * (1.0/256.0);

                    int oh = tmp1;
                    int ow = tmp2;

                    if (mctx->trans_tab_subpixel) {
                        int xpartial = 32.0 * (tmp2 - ow);
                        int ypartial = 32.0 * (tmp1 - oh);

                        if (mctx->wrap) {
                            ow %= w - 1;
                            oh %= h - 1;
                            if (ow < 0)
                                ow += w-1;
                            if (oh < 0)
                                oh += h-1;
                        } else {
                            if (ow < 0) {
                                xpartial = 0;
                                ow = 0;
                            }
                            if (ow >= w-1) {
                                xpartial = 31;
                                ow = w-2;
                            }
                            if (oh < 0) {
                                ypartial = 0;
                                oh = 0;
                            }
                            if (oh >= h-1) {
                                ypartial = 31;
                                oh = h-2;
                            }
                        }

                        *transp++ = (ow + oh * w) | (ypartial << 22) | (xpartial << 27);
                    } else {
                        if (mctx->wrap) {
                            ow %= w;
                            oh %= h;
                            if (ow < 0)
                                ow += w;
                            if (oh < 0)
                                oh += h;
                        } else {
                            if (ow < 0)
                                ow = 0;
                            if (ow >= w)
                                ow = w-1;
                            if (oh < 0)
                                oh = 0;
                            if (oh >= h)
                                oh = h-1;
                        }

                        *transp++ = ow + oh * w;
                    }
                }
        } else if (mctx->effect == 32767 || effect_uses_eval(mctx->effect)) {
            const char *expr = mctx->effect == 32767 ? mctx->effect_expr : descriptions[mctx->effect].expr;
            int is_rect = mctx->effect == 32767 ? mctx->rectangular : descriptions[mctx->effect].uses_rect;

            float max_d = sqrtf(w * w + h * h) / 2.0;
            float divmax_d = 1.0 / max_d;

            if (expr_compile(mctx->expr_context, "__avs_internal_movement", expr))
                return -1;

#define SET(a,b) expr_variable_set(mctx->expr_context, (a), (b))
#define GET(a) expr_variable_get(mctx->expr_context, (a))

            SET("sw", w);
            SET("sh", h);

            float w2 = w/2;
            float h2 = h/2;
            float x_scale = 1.0 / w2;
            float y_scale = 1.0 / h2;

            for (int y = 0; y < h; y++)
                for (int x = 0; x < w; x++) {
                    float xd = x - w2;
                    float yd = y - h2;

                    SET("x", xd * x_scale);
                    SET("y", yd * y_scale);
                    SET("d", sqrtf(xd * xd + yd * yd) * divmax_d);
                    SET("r", atan2f(yd, xd) + M_PI / 2.0);

                    if (expr_execute(mctx->expr_context, "__avs_internal_movement", expr))
                        return -1;

                    float r = GET("r");
                    float d = GET("d");

                    float tmp1, tmp2;
                    /* Note that this condition is the other way round in the
                     * original movement code. */
                    if (is_rect) {
                        tmp1 = (GET("y") + 1.0) * h2;
                        tmp2 = (GET("x") + 1.0) * w2;
                    } else {
                        d *= max_d;
                        r -= M_PI / 2.0;
                        tmp1 = h2 + sinf(r) * d;
                        tmp2 = w2 + cosf(r) * d;
                    }

                    if (mctx->trans_tab_subpixel) {
                        int oh = tmp1;
                        int ow = tmp2;
                        int xpartial = 32.0 * (tmp2 - ow);
                        int ypartial = 32.0 * (tmp1 - oh);

                        if (mctx->wrap) {
                            ow %= w - 1;
                            oh %= h - 1;
                            if (ow < 0)
                                ow += w-1;
                            if (oh < 0)
                                oh += h-1;
                        } else {
                            if (ow < 0) {
                                xpartial = 0;
                                ow = 0;
                            }
                            if (ow >= w-1) {
                                xpartial = 31;
                                ow = w-2;
                            }
                            if (oh < 0) {
                                ypartial = 0;
                                oh = 0;
                            }
                            if (oh >= h-1) {
                                ypartial = 31;
                                oh = h-2;
                            }
                        }

                        *transp++ = (ow + oh * w) | (ypartial << 22) | (xpartial << 27);
                    } else {
                        tmp1 += 0.5;
                        tmp2 += 0.5;
                        int oh = tmp1;
                        int ow = tmp2;
                        if (mctx->wrap) {
                            ow %= w;
                            oh %= h;
                            if (ow < 0)
                                ow += w;
                            if (oh < 0)
                                oh += h;
                        } else {
                            if (ow < 0)
                                ow = 0;
                            if (ow >= w)
                                ow = w-1;
                            if (oh < 0)
                                oh = 0;
                            if (oh >= h)
                                oh = h-1;
                        }

                        *transp++ = ow + oh * w;
                    }
                }

        }
        mctx->effect_exp_ch = 0;
    }

    if ((mctx->new_effect & N_EFFECT_SOURCE_MAP_BEAT) && is_beat)
        mctx->new_effect ^= N_EFFECT_SOURCE_MAP_ON;

    /* This is a very confusing construct.
     * If source map is on and blend is off, the output frame is filled with
     * zeros.  This makes the maximum blend later rather pointless.
     * If blend is on,  the input frame will be copied to the output frame so
     * that max blend works as expected.  Yet the two frames will be blended
     * anyway. */
    if (mctx->new_effect & N_EFFECT_SOURCE_MAP_ON) {
        if (mctx->new_effect & N_EFFECT_BLEND_INPUT)
            memcpy(frame_out, frame, w * h * sizeof(int));
        else
            memset(frame_out, 0, w * h * sizeof(int));
    }

    /* end of smp_begin */
    /* start of smp_render */

    int *input = frame;
    int *output = frame_out;
    int *transp = mctx->trans_tab;

#define OFFSET_MASK ((1<<22)-1)

    switch (mctx->new_effect & (N_EFFECT_BILINEAR|N_EFFECT_SOURCE_MAP_ON)) {
        case N_EFFECT_SOURCE_MAP_ON|N_EFFECT_BILINEAR:
            for (int x = 0; x < w * h; x++) {
                frame_out[transp[x] & OFFSET_MASK] = blend_pixel_max(input[x], frame_out[transp[x] & OFFSET_MASK]);
            }
            break;
        case N_EFFECT_SOURCE_MAP_ON:
            for (int x = 0; x < w * h; x++) {
                frame_out[transp[x]] = blend_pixel_max(input[x], frame_out[transp[x]]);
            }
            break;
        case N_EFFECT_BILINEAR:
            for (int x = 0; x < w * h; x++) {
                int offset = transp[x] & OFFSET_MASK;
                output[x] = blend_pixel_4(frame + offset, w,
                        ((transp[x]>>24) & (31<<3)),
                        ((transp[x]>>19) & (31<<3)));
            }
            break;
        default:
            for (int x = 0; x < w * h; x++) {
                frame_out[x] = frame[transp[x]];
            }
    }
    if (mctx->new_effect & N_EFFECT_BLEND_INPUT)
        blend_block_avg(frame_out, frame_out, frame, w * h);
     /* end of smp_render */

    return 1; /* smp_finish */
}

static void uninit(ComponentContext *ctx)
{
    MovementContext *mctx = ctx->priv;

    FREE((void *)mctx->effect_expr);
    FREE(mctx->trans_tab);

    expr_uninit(mctx->expr_context);
}

Component t_movement = {
    .name = "Trans / Movement",
    .code = 15,
    .priv_size = sizeof(MovementContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
