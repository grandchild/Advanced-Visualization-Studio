/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#if CONFIG_OPENMP
#include <omp.h>
#endif
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int enabled;
    int num_colours;
    struct {
        uint8_t r,g,b;
    } colour[256];
    int pos[256];
    int lut[256];
} Map;

typedef struct {
    int key;
    int mode;
    int cycling;
    int speed;
    int no_skip_fast;
    int blend_adj;
    Map map[8];
} ColourMapContext;

static void fill_lut(int lut[256], Map *map)
{
    /* Fill table below first colour. */
    for (int i = 0; i < map->pos[0]; i++)
        lut[i] = I3_TO_I(map->colour[0].r, map->colour[0].g, map->colour[0].b);

    for (int i = 1; i < map->num_colours; i++) {
        int r_diff = map->colour[i].r - map->colour[i-1].r;
        int g_diff = map->colour[i].g - map->colour[i-1].g;
        int b_diff = map->colour[i].b - map->colour[i-1].b;
        int length = map->pos[i] - map->pos[i-1];

        lut[map->pos[i]] = I3_TO_I(map->colour[i].r,
                map->colour[i].g, map->colour[i].b);
        lut[map->pos[i-1]] = I3_TO_I(map->colour[i-1].r,
                map->colour[i-1].g, map->colour[i-1].b);

        for (int j = 1; j < length; j++) {
            int r = round((j * r_diff) / (double)length);
            int g = round((j * g_diff) / (double)length);
            int b = round((j * b_diff) / (double)length);
            r = CLIP(r + map->colour[i-1].r, 0, 255);
            g = CLIP(g + map->colour[i-1].g, 0, 255);
            b = CLIP(b + map->colour[i-1].b, 0, 255);
            lut[j + map->pos[i-1]] = I3_TO_I(r,g,b);
        }
    }

    /* Fill table above last colour. */
    for (int i = map->pos[map->num_colours-1]; i < 256; i++)
        lut[i] = I3_TO_I(map->colour[map->num_colours-1].r,
                map->colour[map->num_colours-1].g,
                map->colour[map->num_colours-1].b);
}

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    ColourMapContext *clm = ctx->priv;
    int temp;
    int pos = 0;

    R32(clm->key);
    R32(clm->mode);
    R32(clm->cycling);

    if (!(clm->key >= 0 && clm->key <= 5)) {
        ERROR("Unknown key value (%d).\n", clm->key);
    }

    if (!(clm->mode >= 0 && clm->mode <= 9)) {
        ERROR("Unknown mode value (%d).\n", clm->mode);
        return -1;
    }

    R32(temp);
    clm->blend_adj = 0xff - (temp & 0xff); /* To match the output of vis_avs / colormap.ape this value is inverted */
    clm->speed = (temp >> 24) & 0xff;
    clm->no_skip_fast = (temp >> 16) & 0xff;

    for (int i = 0; i < 8; i++) {
        Map *map = &clm->map[i];
        R32(map->enabled);
        R32(map->num_colours);

        if (map->num_colours > 256) {
            WARNING("More than 256 colours in a map are not supported.\n");
            map->num_colours = 256;
        }

        pos += 52; /* Unidentified integers. */
    }

    for (int i = 0; i < 8; i++) {
        Map *map = &clm->map[i];
        for (int j = 0; j < map->num_colours; j++) {
            R32(map->pos[j]);
            R32(temp);
            map->colour[j].r = CR_TO_I(temp);
            map->colour[j].g = CG_TO_I(temp);
            map->colour[j].b = CB_TO_I(temp);
            pos += 4; /* Unidentified integer. */
        }
    }

    for (int i = 0; i < 8; i++) {
        fill_lut(clm->map[i].lut, &clm->map[i]);
    }

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    ColourMapContext *clm = ctx->priv;

    /* FIXME TODO: Make this monstrosity smaller/shorter.
     * Possibly use switch stements.  Possibly factor out the blend mode,
     * blend_block could be used.  Write the colour into the output frame and
     * blend the input into it. */

#define LOOP(KEY, BLEND) \
    for (int y = 0; y < h; y++) { \
        for (int x = 0; x < w; x++) { \
            int key = KEY(frame[y*w+x]); \
            frame[y*w+x] = BLEND(frame[y*w+x],clm->map[0].lut[key],clm->blend_adj); \
        } \
    } \

#define KEY_RED(a) CR_TO_I(a)
#define KEY_GREEN(a) CG_TO_I(a)
#define KEY_BLUE(a) CB_TO_I(a)
#define KEY_RGB_2(a) MIN((CR_TO_I(a)+CG_TO_I(a)+CB_TO_I(a))/2,255)
#define KEY_RGB_3(a) ((CR_TO_I(a)+CG_TO_I(a)+CB_TO_I(a))/3)
#define KEY_MAX(a) MAX3(CR_TO_I(a),CG_TO_I(a),CB_TO_I(a))

/* p is the pixel, l is the lut, a is the adjustable blend value. */
#define BLEND_REP(p,l,a)  (l)
#define BLEND_ADD(p,l,a)  blend_pixel_add(p,l)
#define BLEND_MAX(p,l,a)  blend_pixel_max(p,l)
#define BLEND_MIN(p,l,a)  blend_pixel_min(p,l)
#define BLEND_AVG(p,l,a)  blend_pixel_avg(p,l)
#define BLEND_SUB1(p,l,a) blend_pixel_sub(p,l)
#define BLEND_SUB2(p,l,a) blend_pixel_sub(l,p)
#define BLEND_MUL(p,l,a)  blend_pixel_mul(p,l)
#define BLEND_XOR(p,l,a)  (p) ^ (l)
#define BLEND_ADJ(p,l,a)  blend_pixel_adj(p,l,a)

#if CONFIG_OPENMP_COLLAPSE

    if (clm->key == 0) {
        if (clm->mode == 0) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RED, BLEND_ADJ)
        }
    } else if (clm->key == 1) {
        if (clm->mode == 0) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_GREEN, BLEND_ADJ)
        }
    } else if (clm->key == 2) {
        if (clm->mode == 0) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_BLUE, BLEND_ADJ)
        }
    } else if (clm->key == 3) {
        if (clm->mode == 0) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_2, BLEND_ADJ)
        }
    } else if (clm->key == 4) {
        if (clm->mode == 0) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_MAX, BLEND_ADJ)
        }
    } else if (clm->key == 5) {
        if (clm->mode == 0) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for collapse(2)
           LOOP(KEY_RGB_3, BLEND_ADJ)
        }
    }

#elif CONFIG_OPENMP

    if (clm->key == 0) {
        if (clm->mode == 0) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for
           LOOP(KEY_RED, BLEND_ADJ)
        }
    } else if (clm->key == 1) {
        if (clm->mode == 0) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for
           LOOP(KEY_GREEN, BLEND_ADJ)
        }
    } else if (clm->key == 2) {
        if (clm->mode == 0) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for
           LOOP(KEY_BLUE, BLEND_ADJ)
        }
    } else if (clm->key == 3) {
        if (clm->mode == 0) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for
           LOOP(KEY_RGB_2, BLEND_ADJ)
        }
    } else if (clm->key == 4) {
        if (clm->mode == 0) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for
           LOOP(KEY_MAX, BLEND_ADJ)
        }
    } else if (clm->key == 5) {
        if (clm->mode == 0) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_REP)
        } else if (clm->mode == 1) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_ADD)
        } else if (clm->mode == 2) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_MAX)
        } else if (clm->mode == 3) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_MIN)
        } else if (clm->mode == 4) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_AVG)
        } else if (clm->mode == 5) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_SUB1)
        } else if (clm->mode == 6) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_SUB2)
        } else if (clm->mode == 7) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_MUL)
        } else if (clm->mode == 8) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_XOR)
        } else if (clm->mode == 9) {
           #pragma omp parallel for
           LOOP(KEY_RGB_3, BLEND_ADJ)
        }
    }

#else

         if (clm->key == 0 && clm->mode == 0) LOOP(KEY_RED, BLEND_REP)
    else if (clm->key == 0 && clm->mode == 1) LOOP(KEY_RED, BLEND_ADD)
    else if (clm->key == 0 && clm->mode == 2) LOOP(KEY_RED, BLEND_MAX)
    else if (clm->key == 0 && clm->mode == 3) LOOP(KEY_RED, BLEND_MIN)
    else if (clm->key == 0 && clm->mode == 4) LOOP(KEY_RED, BLEND_AVG)
    else if (clm->key == 0 && clm->mode == 5) LOOP(KEY_RED, BLEND_SUB1)
    else if (clm->key == 0 && clm->mode == 6) LOOP(KEY_RED, BLEND_SUB2)
    else if (clm->key == 0 && clm->mode == 7) LOOP(KEY_RED, BLEND_MUL)
    else if (clm->key == 0 && clm->mode == 8) LOOP(KEY_RED, BLEND_XOR)
    else if (clm->key == 0 && clm->mode == 9) LOOP(KEY_RED, BLEND_ADJ)
    else if (clm->key == 1 && clm->mode == 0) LOOP(KEY_GREEN, BLEND_REP)
    else if (clm->key == 1 && clm->mode == 1) LOOP(KEY_GREEN, BLEND_ADD)
    else if (clm->key == 1 && clm->mode == 2) LOOP(KEY_GREEN, BLEND_MAX)
    else if (clm->key == 1 && clm->mode == 3) LOOP(KEY_GREEN, BLEND_MIN)
    else if (clm->key == 1 && clm->mode == 4) LOOP(KEY_GREEN, BLEND_AVG)
    else if (clm->key == 1 && clm->mode == 5) LOOP(KEY_GREEN, BLEND_SUB1)
    else if (clm->key == 1 && clm->mode == 6) LOOP(KEY_GREEN, BLEND_SUB2)
    else if (clm->key == 1 && clm->mode == 7) LOOP(KEY_GREEN, BLEND_MUL)
    else if (clm->key == 1 && clm->mode == 8) LOOP(KEY_GREEN, BLEND_XOR)
    else if (clm->key == 1 && clm->mode == 9) LOOP(KEY_GREEN, BLEND_ADJ)
    else if (clm->key == 2 && clm->mode == 0) LOOP(KEY_BLUE, BLEND_REP)
    else if (clm->key == 2 && clm->mode == 1) LOOP(KEY_BLUE, BLEND_ADD)
    else if (clm->key == 2 && clm->mode == 2) LOOP(KEY_BLUE, BLEND_MAX)
    else if (clm->key == 2 && clm->mode == 3) LOOP(KEY_BLUE, BLEND_MIN)
    else if (clm->key == 2 && clm->mode == 4) LOOP(KEY_BLUE, BLEND_AVG)
    else if (clm->key == 2 && clm->mode == 5) LOOP(KEY_BLUE, BLEND_SUB1)
    else if (clm->key == 2 && clm->mode == 6) LOOP(KEY_BLUE, BLEND_SUB2)
    else if (clm->key == 2 && clm->mode == 7) LOOP(KEY_BLUE, BLEND_MUL)
    else if (clm->key == 2 && clm->mode == 8) LOOP(KEY_BLUE, BLEND_XOR)
    else if (clm->key == 2 && clm->mode == 9) LOOP(KEY_BLUE, BLEND_ADJ)
    else if (clm->key == 3 && clm->mode == 0) LOOP(KEY_RGB_2, BLEND_REP)
    else if (clm->key == 3 && clm->mode == 1) LOOP(KEY_RGB_2, BLEND_ADD)
    else if (clm->key == 3 && clm->mode == 2) LOOP(KEY_RGB_2, BLEND_MAX)
    else if (clm->key == 3 && clm->mode == 3) LOOP(KEY_RGB_2, BLEND_MIN)
    else if (clm->key == 3 && clm->mode == 4) LOOP(KEY_RGB_2, BLEND_AVG)
    else if (clm->key == 3 && clm->mode == 5) LOOP(KEY_RGB_2, BLEND_SUB1)
    else if (clm->key == 3 && clm->mode == 6) LOOP(KEY_RGB_2, BLEND_SUB2)
    else if (clm->key == 3 && clm->mode == 7) LOOP(KEY_RGB_2, BLEND_MUL)
    else if (clm->key == 3 && clm->mode == 8) LOOP(KEY_RGB_2, BLEND_XOR)
    else if (clm->key == 3 && clm->mode == 9) LOOP(KEY_RGB_2, BLEND_ADJ)
    else if (clm->key == 4 && clm->mode == 0) LOOP(KEY_MAX, BLEND_REP)
    else if (clm->key == 4 && clm->mode == 1) LOOP(KEY_MAX, BLEND_ADD)
    else if (clm->key == 4 && clm->mode == 2) LOOP(KEY_MAX, BLEND_MAX)
    else if (clm->key == 4 && clm->mode == 3) LOOP(KEY_MAX, BLEND_MIN)
    else if (clm->key == 4 && clm->mode == 4) LOOP(KEY_MAX, BLEND_AVG)
    else if (clm->key == 4 && clm->mode == 5) LOOP(KEY_MAX, BLEND_SUB1)
    else if (clm->key == 4 && clm->mode == 6) LOOP(KEY_MAX, BLEND_SUB2)
    else if (clm->key == 4 && clm->mode == 7) LOOP(KEY_MAX, BLEND_MUL)
    else if (clm->key == 4 && clm->mode == 8) LOOP(KEY_MAX, BLEND_XOR)
    else if (clm->key == 4 && clm->mode == 9) LOOP(KEY_MAX, BLEND_ADJ)
    else if (clm->key == 5 && clm->mode == 0) LOOP(KEY_RGB_3, BLEND_REP)
    else if (clm->key == 5 && clm->mode == 1) LOOP(KEY_RGB_3, BLEND_ADD)
    else if (clm->key == 5 && clm->mode == 2) LOOP(KEY_RGB_3, BLEND_MAX)
    else if (clm->key == 5 && clm->mode == 3) LOOP(KEY_RGB_3, BLEND_MIN)
    else if (clm->key == 5 && clm->mode == 4) LOOP(KEY_RGB_3, BLEND_AVG)
    else if (clm->key == 5 && clm->mode == 5) LOOP(KEY_RGB_3, BLEND_SUB1)
    else if (clm->key == 5 && clm->mode == 6) LOOP(KEY_RGB_3, BLEND_SUB2)
    else if (clm->key == 5 && clm->mode == 7) LOOP(KEY_RGB_3, BLEND_MUL)
    else if (clm->key == 5 && clm->mode == 8) LOOP(KEY_RGB_3, BLEND_XOR)
    else if (clm->key == 5 && clm->mode == 9) LOOP(KEY_RGB_3, BLEND_ADJ)

#endif

    return 0;
}

Component t_colourmap = {
    .name = "Trans / Colour Map",
    .signature = "Color Map",
    .priv_size = sizeof(ColourMapContext),
    .load_config = load_config,
    .render = render,
};
