/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "expressions.h"
#include "pixel.h"

typedef struct {
    const char *expr_init;
    const char *expr_frame;
    const char *expr_beat;
    const char *expr_pixel;
    void *expr_context;
    int last_w, last_h, last_x_res, last_y_res;
    int x_res, y_res;
    int *m_tab;
    int inited;
    int buffer_n;
    int subpixel;
    int rect;
    int blend;
    int wrap;
    int no_move;
    int *data[3];
} DynamicMovementContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    DynamicMovementContext *dyn = ctx->priv;
    int pos = 0;

    void *expr_ctx = expr_init(ctx->actx);
    if (!expr_ctx)
        return -1;

    if (*buf == 1) {
        int ret = 0;
        pos++;

        ret = expr_load_rstring(expr_ctx, &dyn->expr_pixel, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &dyn->expr_frame, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &dyn->expr_beat, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &dyn->expr_init, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;
    } else {
        /* read fixed len */
            ERROR("fixed-length expressions not yet implemented\n");
    }

    R32(dyn->subpixel);
    R32(dyn->rect);
    R32(dyn->x_res);
    R32(dyn->y_res);
    R32(dyn->blend);
    R32(dyn->wrap);
    R32(dyn->buffer_n);
    R32(dyn->no_move);

    if (expr_compile(expr_ctx, "__avs_internal_init", dyn->expr_init)
            || expr_compile(expr_ctx, "__avs_internal_frame", dyn->expr_frame)
            || expr_compile(expr_ctx, "__avs_internal_beat", dyn->expr_beat)
            || expr_compile(expr_ctx, "__avs_internal_pixel", dyn->expr_pixel))
        return -1;

    dyn->expr_context = expr_ctx;

    return 0;
}

static void print_config(ComponentContext *ctx, int indent)
{

#define INDENT() fprintf(stderr, "%*c", indent * 4, ' ')

#define PRINT_BOOL(a, b) \
    INDENT(); \
    fprintf(stderr, a " = %s\n", (b) ? "true" : "false")

#define PRINT(a, ...) \
    INDENT(); \
    fprintf(stderr, a, ##__VA_ARGS__)

#define PRINT1(a) \
    INDENT(); \
    fprintf(stderr, a);

#define PRINT2(a, b) \
    INDENT(); \
    fprintf(stderr, a, b);

    DynamicMovementContext *dyn = ctx->priv;

    PRINT2("expression init = %s\n", dyn->expr_init);
    PRINT2("expression frame = %s\n", dyn->expr_frame);
    PRINT2("expression beat = %s\n", dyn->expr_beat);
    PRINT2("expression pixel = %s\n", dyn->expr_pixel);
    PRINT_BOOL("rectangular coordinates", dyn->rect);
    PRINT_BOOL("wrap", dyn->wrap);
    PRINT("x_res = %d, y_res = %d\n", dyn->x_res, dyn->y_res);

    PRINT_BOOL("subpixel", dyn->subpixel);

    PRINT_BOOL("blend", dyn->blend);
    PRINT2("blend source = %d\n", dyn->buffer_n);
    PRINT_BOOL("no movement", dyn->no_move);
}


static int init_table(DynamicMovementContext *dyn, int w, int h, int is_beat)
{
    int x_res = CLIP(dyn->x_res + 1, 2, 256);
    int y_res = CLIP(dyn->y_res + 1, 2, 256);

    if (dyn->last_w != w || dyn->last_h != h || !dyn->m_tab || dyn->last_x_res != x_res || dyn->last_y_res != y_res) {
        dyn->last_w = w;
        dyn->last_h = h;
        dyn->last_x_res = x_res;
        dyn->last_y_res = y_res;

        if (dyn->m_tab)
            free(dyn->m_tab);

        dyn->m_tab = malloc((x_res * y_res * 3 + (x_res * 6 + 6)) * sizeof(int));
        if (!dyn->m_tab)
            return -1;
    }

    int w_adj, h_adj;

    if (dyn->subpixel) {
        w_adj = (w-2) << 16;
        h_adj = (h-2) << 16;
    } else {
        w_adj = (w-1) << 16;
        h_adj = (h-1) << 16;
    }

#define SET(a,b) expr_variable_set(dyn->expr_context, (a), (b))
#define GET(a) expr_variable_get(dyn->expr_context, (a))

    SET("w", w);
    SET("h", h);
    SET("b", is_beat ? 1.0 : 0.0);
    SET("alpha", 0.5);

    if (!dyn->inited) {
        if(expr_execute(dyn->expr_context, "__avs_internal_init", dyn->expr_init))
            return -1;
        dyn->inited = 1;
    }

    if (expr_execute(dyn->expr_context, "__avs_internal_frame", dyn->expr_frame))
        return -1;

    if (is_beat)
        if (expr_execute(dyn->expr_context, "__avs_internal_beat", dyn->expr_beat))
            return -1;

    double max_screen_d = sqrt(w*w + h*h) / 2.0;
    int xc_dpos = (w << 16) / (x_res - 1);
    int yc_dpos = (h << 16) / (y_res - 1);
    int yc_pos = 0;
    int *tabptr = dyn->m_tab;

    for (int y = 0; y < y_res; y++) {
        int xc_pos = 0;

        for (int x = 0; x < x_res; x++) {
            double xd = (xc_pos - (w * 32768.0)) / 65536.0;
            double yd = (yc_pos - (h * 32768.0)) / 65536.0;

            xc_pos += xc_dpos;

            SET("x", xd * 2.0 / w);
            SET("y", yd * 2.0 / h);
            SET("d", sqrt(xd*xd + yd*yd) / max_screen_d);
            SET("r", atan2(yd, xd) + (M_PI / 2.0));

            if (expr_execute(dyn->expr_context, "__avs_internal_pixel", dyn->expr_pixel))
                return -1;

            int tmp1, tmp2;

            if (dyn->rect) {
                tmp1 = (GET("x") + 1.0) * w * 32768.0;
                tmp2 = (GET("y") + 1.0) * h * 32768.0;
            } else {
                double d = GET("d") * max_screen_d * 65536.0;
                double r = GET("r") - (M_PI / 2.0);
                tmp1 = w * 32768.0 + cos(r) * d;
                tmp2 = h * 32768.0 + sin(r) * d;
            }

            if (!dyn->wrap) {
                tmp1 = CLIP(tmp1, 0, w_adj);
                tmp2 = CLIP(tmp2, 0, h_adj);
            }

            *tabptr++ = tmp1;
            *tabptr++ = tmp2;
            double alpha = GET("alpha");
            *tabptr++ = CLIP(alpha, 0.0, 1.0) * 255.0 * 65536.0;
        }

        yc_pos += yc_dpos;
    }

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    DynamicMovementContext *dyn = ctx->priv;

    /* The table holds a set of precalculated movement points which split up the
     * image into the user defined x_res by y_res grid.  The first two x and y
     * points bound the first grid.  The movement of each pixel between these
     * points is interpolated from the precalculated values. */

    if (init_table(dyn, w, h, is_beat))
        return -1;

    int *source = (!dyn->buffer_n) ? frame : ctx->actx->global_buffer[dyn->buffer_n - 1];
    if (!source) {
        /* FIXME: A preset might try to read from an empty buffer. */
        ERROR("Trying to use unallocated buffer (%d) as source. This might be "
              "deliberate, please report if you see this message.\n",
              dyn->buffer_n);
        return -1;
    }

    int x_res = CLIP(dyn->x_res + 1, 2, 256);
    int y_res = CLIP(dyn->y_res + 1, 2, 256);

    int w_adj, h_adj;

    if (dyn->subpixel) {
        w_adj = (w-2) << 16;
        h_adj = (h-2) << 16;
    } else {
        w_adj = (w-1) << 16;
        h_adj = (h-1) << 16;
    }

    int *inter_tab = dyn->m_tab + x_res * y_res * 3;
    int *rd_tab = dyn->m_tab;
    int xc_dpos = (w << 16) / (x_res - 1);
    int yc_dpos = (h << 16) / (y_res - 1);
    int yc_pos = 0;
    int ly_pos = 0;

    for (int y = h; y > 0; /* do nothing */) {
        yc_pos += yc_dpos;

        int y_seek = (yc_pos >> 16) - ly_pos;
        if (!y_seek)
            return 1;

        ly_pos = yc_pos >> 16;

        for (int x = 0; x < x_res; x++) {
            int tmp1 = rd_tab[0];
            int tmp2 = rd_tab[1];
            int tmp3 = rd_tab[2];

            inter_tab[x*6+0] = tmp1;
            inter_tab[x*6+1] = tmp2;
            inter_tab[x*6+2] = (rd_tab[x_res*3+0] - tmp1) / y_seek;
            inter_tab[x*6+3] = (rd_tab[x_res*3+1] - tmp2) / y_seek;
            inter_tab[x*6+4] = tmp3;
            inter_tab[x*6+5] = (rd_tab[x_res*3+2] - tmp3) / y_seek;

            rd_tab += 3;
        }

        y_seek = MIN(y_seek, y);
        y -= y_seek;

        for (/* do nothing */; y_seek > 0; y_seek--) {
            int lx_pos = 0, xc_pos = 0;
            int *seek_tab = inter_tab;

            for (int x = w; x > 0; /*do nothing*/) {
                xc_pos += xc_dpos;

                int x_seek = (xc_pos >> 16) - lx_pos;
                if (!x_seek)
                    return 1;

                lx_pos = xc_pos >> 16;
                int xp = seek_tab[0];
                int yp = seek_tab[1];
                int ap = seek_tab[4];
                int d_x = (seek_tab[6] - xp) / x_seek;
                int d_y = (seek_tab[7] - yp) / x_seek;
                int d_a = (seek_tab[10] - ap) / x_seek;
                seek_tab[0] += seek_tab[2];
                seek_tab[1] += seek_tab[3];
                seek_tab[4] += seek_tab[5];
                seek_tab += 6;

                x_seek = MIN(x_seek, x);
                x -= x_seek;

                if (x_seek <= 0)
                    continue;

#define LOOPS(TOP) \
    if (dyn->blend && dyn->subpixel) while (x_seek--) { \
        TOP \
        *frame_out = blend_pixel_4_16(source + (xp>>16) + w * (yp>>16), w, xp, yp); \
        *frame_out = blend_pixel_adj(*frame_out, *frame, ap>>16); \
        ap += d_a; \
        xp += d_x; \
        yp += d_y; \
        frame_out++; \
        frame++; \
    } \
    else if (dyn->blend) while (x_seek--) { \
        TOP \
        *frame_out = *(source + (xp>>16) + w * (yp>>16)); \
        *frame_out = blend_pixel_adj(*frame_out, *frame, ap>>16); \
        ap += d_a; \
        xp += d_x; \
        yp += d_y; \
        frame_out++; \
        frame++; \
    } \
    else if (dyn->subpixel) while (x_seek--) { \
        TOP \
        *frame_out = blend_pixel_4_16(source + (xp>>16) + w * (yp>>16), w, xp, yp); \
        xp += d_x; \
        yp += d_y; \
        frame_out++; \
    } \
    else while (x_seek--) { \
        TOP \
        *frame_out = *(source + (xp>>16) + w * (yp>>16)); \
        xp += d_x; \
        yp += d_y; \
        frame_out++; \
    }

                if (dyn->no_move) {
                    if (dyn->buffer_n) while (x_seek--) {
                        *frame_out = blend_pixel_adj(*source, *frame, ap >> 16);
                        ap += d_a;
                        frame_out++;
                        frame++;
                        source++;
                    }
                    else while (x_seek--) {
                        *frame_out = blend_pixel_adj(0, *frame, ap >> 16);
                        ap += d_a;
                        frame_out++;
                        frame++;
                    }
                } else if (dyn->wrap) {
                    xp %= w_adj;
                    yp %= h_adj;
                    d_x = CLIP(d_x, -w_adj + 1, w_adj - 1);
                    d_y = CLIP(d_y, -h_adj + 1, h_adj - 1);

                    LOOPS(if (xp < 0) xp += w_adj; else if (xp >= w_adj) xp-=w_adj; if (yp < 0) yp += h_adj; else if (yp >= h_adj) yp-=h_adj;)
                } else {
                    LOOPS(xp = CLIP(xp, 0, w_adj-1); yp = CLIP(yp, 0, h_adj-1);)
                }
            }

            seek_tab[0] += seek_tab[2];
            seek_tab[1] += seek_tab[3];
            seek_tab[4] += seek_tab[5];
        }
    }

    return 1;
}

static void uninit(ComponentContext *ctx)
{
    DynamicMovementContext *dyn = ctx->priv;

    FREE((void *)dyn->expr_init);
    FREE((void *)dyn->expr_frame);
    FREE((void *)dyn->expr_beat);
    FREE((void *)dyn->expr_pixel);
    FREE(dyn->m_tab);

    expr_uninit(dyn->expr_context);
}

Component t_dynamicmovement = {
    .name = "Trans / Dynamic Movement",
    .code = 43,
    .priv_size = sizeof(DynamicMovementContext),
    .load_config = load_config,
    .print_config = print_config,
    .render = render,
    .uninit = uninit,
};
