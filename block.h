/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_BLOCK
#define AVS_HEADER_BLOCK 1

void blend_block(int *output, int *a, int *b, int pixels, int blend_mode);

extern void (*blend_block_avg)(int *output, int *a, int *b, int pixels);
extern void (*blend_block_add)(int *output, int *a, int *b, int pixels);
extern void (*blend_block_sub)(int *output, int *a, int *b, int pixels);
extern void (*blend_block_xor)(int *output, int *a, int *b, int pixels);
extern void (*blend_block_max)(int *output, int *a, int *b, int pixels);
extern void (*blend_block_min)(int *output, int *a, int *b, int pixels);
extern void (*blend_block_mul)(int *output, int *a, int *b, int pixels);
extern void (*blend_block_adj)(int *output, int *a, int *b, int pixels, int v);

void blend_block_const(int *output, int *a, int colour, int pixels, int blend_mode);

extern void (*blend_block_const_avg)(int *output, int *a, int colour, int pixels);
extern void (*blend_block_const_add)(int *output, int *a, int colour, int pixels);
extern void (*blend_block_const_sub1)(int *output, int *a, int colour, int pixels);
extern void (*blend_block_const_sub2)(int *output, int colour, int *a, int pixels);
extern void (*blend_block_const_xor)(int *output, int *a, int colour, int pixels);
extern void (*blend_block_const_max)(int *output, int *a, int colour, int pixels);
extern void (*blend_block_const_min)(int *output, int *a, int colour, int pixels);
extern void (*blend_block_const_mul)(int *output, int *a, int colour, int pixels);
extern void (*blend_block_const_adj)(int *output, int *a, int colour, int pixels, int v);

#endif /* AVS_HEADER_BLOCK */
