/*
 * Copyright (c) 2015 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#define UNICODE 1
#define _UNICODE 1

#include <stdio.h>
#include <stdint.h>
#include <windows.h>
#include <commctrl.h>

#include "attributes.h"
#include "avs.h"
#include "winamp/vis.h"

#define JD_AVS_description "JD Advanced Visualization Studio v0"

static winampVisModule JD_AVS;
static winampVisHeader JD_AVS_header;

static struct {
    int width, height, ms_per_frame;
    AVSContext *avs_context;
    AVSDataContext avsdc;
    char filename[MAX_PATH];
    BITMAPINFO bitmap_info;
    HWND window;
    HFONT font;
    int is_running, is_configured;
    wchar_t base_path[MAX_PATH];
} JD_AVS_options;

static void clear_options(void)
{
    avs_uninit(JD_AVS_options.avs_context);
    memset(&JD_AVS_options, 0, sizeof(JD_AVS_options));
}

/*
 * Config window.
 */

enum config_control_ids {
    CONFIG_PRESET_EDITBOX = 100,
    CONFIG_PRESET_BUTTON,
    CONFIG_WIDTH_EDITBOX,
    CONFIG_HEIGHT_EDITBOX,
    CONFIG_SPEED_EDITBOX,
    CONFIG_APPLY_BUTTON,
};

static void create_control(HWND window, RECT r, const wchar_t *class,
        int style, int exstyle, const wchar_t *text, enum config_control_ids id,
        HINSTANCE instance)
{
    HWND control = CreateWindowEx(exstyle, class, text,
            style | WS_CHILD | WS_VISIBLE,
            r.left, r.top, r.right, r.bottom,
            window, (HMENU)id, instance, 0);
    SendMessage(control, WM_SETFONT, (WPARAM)JD_AVS_options.font, 1);
}

static void create_config_controls(HWND w, RECT r, HINSTANCE instance)
{
    r.left += 11;
    r.top += 11;
    r.right -= 22;
    r.bottom -= 22;

#define R(l,t,w,h) (RECT){r.left+(l), r.top+(t), (w), (h)}

    create_control(w, R(0, 0, r.right, 16+23+10),
            WC_BUTTON, BS_GROUPBOX, 0, L"Preset", 0, instance);

    r = R(9, 16, r.right - 18, r.bottom - 16);

    create_control(w, R(0, 2, r.right-80, 19),
            WC_EDIT, 0, WS_EX_CLIENTEDGE, L"Filename goes here (unused)",
            CONFIG_PRESET_EDITBOX, instance);
    create_control(w, R(r.right - 75, 0, 75, 23),
            WC_BUTTON, 0, 0, L"Open Preset",
            CONFIG_PRESET_BUTTON, instance);

    r = R(-9, 33, r.right + 18, r.bottom - 33);

    create_control(w, R(0, 0, r.right, 16+19+10),
            WC_BUTTON, BS_GROUPBOX, 0, L"Render settings", 0, instance);

    r = R(9, 16, r.right - 18, r.bottom - 16);

    create_control(w, R(0, 3, 40, 13),
            WC_STATIC, 0, 0, L"Width", 0, instance);
    create_control(w, R(40, 0, 40, 19),
            WC_EDIT, ES_NUMBER, WS_EX_CLIENTEDGE, L"480",
            CONFIG_WIDTH_EDITBOX, instance);

    create_control(w, R(80+7, 3, 40, 13),
            WC_STATIC, 0, 0, L"Height", 0, instance);
    create_control(w, R(120+7, 0, 40, 19),
            WC_EDIT, ES_NUMBER, WS_EX_CLIENTEDGE, L"360",
            CONFIG_HEIGHT_EDITBOX, instance);

    create_control(w, R(160+14, 3, 100, 13),
            WC_STATIC, 0, 0, L"Frame length (ms)", 0, instance);
    create_control(w, R(260+14, 0, 40, 19),
            WC_EDIT, ES_NUMBER, WS_EX_CLIENTEDGE, L"33",
            CONFIG_SPEED_EDITBOX, instance);

    r = R(-9, 29, r.right + 18, r.bottom - 29);

    create_control(w, R(r.right - 75, r.bottom - 23, 75, 23),
            WC_BUTTON, 0, 0, L"Apply", CONFIG_APPLY_BUTTON, instance);
}

static LRESULT CALLBACK config_window_procedure(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
{
    LRESULT ret = 0;

    switch (message) {
        case WM_COMMAND: {
            switch (LOWORD(wparam)) {
                case CONFIG_APPLY_BUTTON: {
                    int w, h;
                    w = JD_AVS_options.width = GetDlgItemInt(window,
                            CONFIG_WIDTH_EDITBOX, 0, 0);
                    h = JD_AVS_options.height = GetDlgItemInt(window,
                            CONFIG_HEIGHT_EDITBOX, 0, 0);
                    JD_AVS_options.ms_per_frame = GetDlgItemInt(window,
                            CONFIG_SPEED_EDITBOX, 0, 0);
                    /* TODO: get filename from window. */

                    JD_AVS_options.bitmap_info = (BITMAPINFO){{
                        .biSize = sizeof(JD_AVS_options.bitmap_info.bmiHeader),
                        .biWidth = w,
                        .biHeight = -h,
                        .biPlanes = 1,
                        .biBitCount = 32,
                        .biCompression = BI_RGB,
                    },{}}; /* Silence "missing initializer" warning. */

                    JD_AVS_options.is_configured = 1;
                    DestroyWindow(window);
                } break;

                case CONFIG_PRESET_BUTTON: {
                    OPENFILENAMEA open_file = {
                        .lStructSize = sizeof(open_file),
                        .hwndOwner = window,
                        .lpstrFilter = "AVS presets\0*.avs\0All files\0*.*\0",
                        .lpstrFile = JD_AVS_options.filename,
                        .nMaxFile = MAX_PATH,
                        .Flags = OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST,
                    };

                    GetOpenFileNameA(&open_file);
                } break;
            }
        } break;

        case WM_DESTROY: {
            DeleteObject(JD_AVS_options.font);
            JD_AVS_options.font = 0;
            PostQuitMessage(0);
        } break;

        default:
            ret = DefWindowProc(window, message, wparam, lparam);
    }

    return ret;
}

static void JD_AVS_config(winampVisModule *this)
{
    if (JD_AVS_options.is_configured || JD_AVS_options.is_running) {
        MessageBox(this->hwndParent,
                L"This plugin cannot be reconfigured while running.\n"
                 "Please stop it then try again.", 0, MB_OK);
        return;
    }

    WNDCLASSEX config_window_class = {
        .cbSize = sizeof(config_window_class),
            .style = CS_HREDRAW | CS_VREDRAW,
        .lpfnWndProc = config_window_procedure,
        .hInstance = this->hDllInstance,
        .hCursor = LoadCursor(0, IDC_ARROW),
        .hbrBackground = (HBRUSH)(COLOR_3DFACE + 1),
        .lpszClassName = L"" JD_AVS_description " config window",
    };

    if (!GetClassInfoEx(this->hDllInstance,
                config_window_class.lpszClassName,
                &config_window_class))
        if (!RegisterClassEx(&config_window_class)) {
            clear_options();
            return;
        }

    HWND window = CreateWindowEx(0,
                config_window_class.lpszClassName,
                config_window_class.lpszClassName,
                WS_CAPTION | WS_SYSMENU | WS_VISIBLE,
                CW_USEDEFAULT, CW_USEDEFAULT, 480, 360,
                this->hwndParent, 0, this->hDllInstance, 0);
    if (!window) {
        clear_options();
        return;
    }

    NONCLIENTMETRICS ncm = {
        .cbSize = sizeof(ncm),
    };
    SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(ncm), &ncm, 0);
    JD_AVS_options.font =  CreateFontIndirect(&ncm.lfMessageFont);

    RECT r;
    GetClientRect(window, &r);
    create_config_controls(window, r, this->hDllInstance);


    MSG message;
    while (GetMessage(&message, 0, 0, 0)) {
        TranslateMessage(&message);
        DispatchMessage(&message);
    }

    return;
}

/*
 * Display window.
 */

enum display_control_ids {
    DISPLAY_STATUSBAR = 100,
};

static void paint_frame(HWND window, void *frame)
{
    RECT r_main, r_status;
    HWND status_bar = GetDlgItem(window, DISPLAY_STATUSBAR);
    GetClientRect(window, &r_main);
    GetWindowRect(status_bar, &r_status);

    int x = r_main.left;
    int y = r_main.top;
    int w = r_main.right - r_main.left;
    int h = r_main.bottom - r_main.top - (r_status.bottom - r_status.top);

    HDC device = GetDC(window);
    StretchDIBits(device,
            x, y, w, h,
            0, 0, JD_AVS_options.width, JD_AVS_options.height,
            frame, &JD_AVS_options.bitmap_info,
            DIB_RGB_COLORS, SRCCOPY);
    ReleaseDC(window, device);
}

static void update_speed_display(HWND window)
{
#define lengthof(a) ((int)(sizeof(a) / sizeof((a)[0])))

    static int tick_counts[64];
    static int tc_pos;
    static int tc_prev;

    int tc_last = tick_counts[tc_pos];
    int tc_this = GetTickCount();

    tick_counts[tc_pos] = tc_this;
    tc_pos = (tc_pos + 1) % lengthof(tick_counts);

    if (tc_this - tc_prev >= 100) {
        double fps = (lengthof(tick_counts) * 1000.0) / (tc_this - tc_last);

        wchar_t text[256];
        _snwprintf(text, 256, L"%dx%d @ %0.1ffps",
                JD_AVS_options.width, JD_AVS_options.height, fps);
        SetDlgItemText(window, DISPLAY_STATUSBAR, text);

        tc_prev = tc_this;
    }
}

static LRESULT CALLBACK window_procedure(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
{
    LRESULT ret = 0;

    switch (message) {
        case WM_SIZE: {
            RECT r_status;
            HWND status_bar = GetDlgItem(window, DISPLAY_STATUSBAR);
            GetWindowRect(status_bar, &r_status);
            MoveWindow(status_bar,
                    0, HIWORD(lparam) - (r_status.bottom - r_status.top),
                    LOWORD(lparam), r_status.bottom - r_status.top, 1);
        } break;

        case WM_CLOSE: {
            JD_AVS_options.is_running = 0;
            DestroyWindow(window);
        } break;

        default:
            ret = DefWindowProc(window, message, wparam, lparam);
    }

    return ret;
}

static void get_base_path(wchar_t base_path[MAX_PATH], HMODULE handle)
{
    GetModuleFileName(handle, base_path, MAX_PATH);
    wchar_t *p = base_path + wcslen(base_path);
    while (p > base_path && *p != L'\\')
        p--;
    *p = 0;
}

static int JD_AVS_init(winampVisModule *this)
{
    get_base_path(JD_AVS_options.base_path, this->hDllInstance);

    /* TODO: make the plugin actually store data between pressing the
     * "Configure" and "Start" buttons.  And store it between sessions. */
    if (!JD_AVS_options.is_configured)
        JD_AVS_config(this);

    if (avs_init(&JD_AVS_options.avs_context, JD_AVS_options.filename, 0)) {
        char text[512] = "Sorry but there was an error while loading the "
                         "preset you selected:\n\"";
        strcat(text, JD_AVS_options.filename);
        strcat(text, "\"\nPlease report this.");
        MessageBoxA(this->hwndParent, text, 0, MB_OK);
        return 1;
    }

    WNDCLASSEXW window_class = {
        .cbSize = sizeof(window_class),
        .style = CS_HREDRAW | CS_VREDRAW,
        .lpfnWndProc = window_procedure,
        .hInstance = this->hDllInstance,
        .hCursor = LoadCursor(NULL, IDC_ARROW),
        .hbrBackground = GetStockObject(BLACK_BRUSH),
        .lpszClassName = L"" JD_AVS_description,
    };

    if (!GetClassInfoEx(this->hDllInstance,
                window_class.lpszClassName,
                &window_class))
        if (!RegisterClassEx(&window_class))
            return 1;

    HWND window = JD_AVS_options.window = CreateWindowExW(0,
            window_class.lpszClassName,
            window_class.lpszClassName,
            WS_VISIBLE | WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
            this->hwndParent, NULL, this->hDllInstance, NULL);
    if (!window)
        return 1;

    HWND status_bar = CreateWindowExW(0, STATUSCLASSNAME, L"Status Bar",
            WS_CHILD | WS_VISIBLE, 0, 0, 0, 0, window,
            (HMENU)DISPLAY_STATUSBAR, this->hDllInstance, 0);
    if (!status_bar)
        return 1;

    RECT r_status;
    GetWindowRect(status_bar, &r_status);

    RECT r_main = {0, 0, JD_AVS_options.width, JD_AVS_options.height};
    AdjustWindowRectEx(&r_main, WS_OVERLAPPEDWINDOW, 0, 0);
    SetWindowPos(window, 0, 0, 0,
            r_main.right - r_main.left,
            r_main.bottom - r_main.top + (r_status.bottom - r_status.top),
            SWP_NOMOVE | SWP_NOZORDER);

    JD_AVS_options.is_running = 1;

    return 0;
}

unaligned static int JD_AVS_render(winampVisModule *this)
{
    uint8_t center_data[2][576];

    for (int i = 0; i < 576; i++) {
        this->waveformData[0][i] ^= 128;
        this->waveformData[1][i] ^= 128;

        center_data[0][i] = (this->waveformData[0][i] +
                this->waveformData[1][i] + 1) / 2;

        center_data[1][i] = (this->spectrumData[0][i] +
                this->spectrumData[1][i] + 1) / 2;
    }

    AVSDataContext avsdc = {
        .waveform = {
            this->waveformData[0],
            this->waveformData[1],
            center_data[0]
        },

        .spectrum = {
            this->spectrumData[0],
            this->spectrumData[1],
            center_data[1]
        },

        .width = JD_AVS_options.width,
        .height = JD_AVS_options.height,
    };

    int ret = avs_render_frame(JD_AVS_options.avs_context,
            &avsdc, GetTickCount());

    paint_frame(JD_AVS_options.window,
            avs_get_frame(JD_AVS_options.avs_context));
    update_speed_display(JD_AVS_options.window);

    static DWORD tick_now, tick_prev;
    tick_now = GetTickCount();

    int tick_diff = tick_now - tick_prev;

    if (tick_diff < JD_AVS_options.ms_per_frame)
        Sleep(JD_AVS_options.ms_per_frame - tick_diff);

    tick_prev = tick_now;

    return ret || !JD_AVS_options.is_running;
}

static void JD_AVS_quit(winampVisModule *this)
{
    clear_options();
    UnregisterClass(L"" JD_AVS_description, this->hDllInstance);
    UnregisterClass(L"" JD_AVS_description " config window", this->hDllInstance);
}

static winampVisModule *JD_AVS_get_module(int which)
{
    if (!which)
        return &JD_AVS;
    return NULL;
}

static winampVisHeader JD_AVS_header = {
    .version = VIS_HDRVER,
    .description = JD_AVS_description,
    .getModule = JD_AVS_get_module,
};

static winampVisModule JD_AVS = {
    .description = JD_AVS_description,
    .spectrumNch = 2,
    .waveformNch = 2,
    .Config = JD_AVS_config,
    .Init = JD_AVS_init,
    .Render = JD_AVS_render,
    .Quit = JD_AVS_quit,
};

__declspec(dllexport) winampVisHeader* winampVisGetHeader(void)
{
    return &JD_AVS_header;
}

#if 0
BOOL __stdcall DllMainCRTStartup(HANDLE hInst, ULONG ul_reason_for_call, LPVOID lpReserved)
{
    return TRUE;
}
#endif
