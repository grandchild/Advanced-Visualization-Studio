/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "avs.h"
#include "common.h"
#include "components.h"
#include "expressions.h"

#include "lua-helper.h"

static double get_vis_internal(const struct three_channel *vis_source,
        int center, int width, const int channel, const int x)
{
    if (channel < 0 || channel > 2)
        return 0.0;

    if (width < 1)
        width = 1;

    center -= width / 2;
    if (center < 0) {
        width += center;
        center = 0;
    }

    if (center > 575)
        center = 575;

    if (center + width > 576)
        width = 576 - center;

    uint8_t *vis_data;
    if (channel == 0)
        vis_data = vis_source->center;
    else if (channel == 1)
        vis_data = vis_source->left;
    else
        vis_data = vis_source->right;

    double accumulator = 0.0;
    for (int left = center, right = center + width; left < right; left++)
        accumulator += vis_data[left] - x;

    return accumulator / (width * 127.5);
}

static int get_spectrum(lua_State *L)
{
    lua_pushnumber(L, get_vis_internal(
                lua_touserdata(L, lua_upvalueindex(1)),
                lua_tonumber(L, 1) * 576.0,
                lua_tonumber(L, 2) * 576.0,
                lua_tonumber(L, 3) + 0.5,
                0));
    return 1;
}

static int get_waveform(lua_State *L)
{
    lua_pushnumber(L, get_vis_internal(
                lua_touserdata(L, lua_upvalueindex(1)),
                lua_tonumber(L, 1) * 576.0,
                lua_tonumber(L, 2) * 576.0,
                lua_tonumber(L, 3) + 0.5,
                128));
    return 1;
}

static int global_megabuf(lua_State *L)
{
    int args = lua_gettop(L);
    double *buffer = lua_touserdata(L, lua_upvalueindex(1));
    int which = lua_tonumber(L, 1) + 0.00001;

    /* Set a value in the global megabuffer. */
    if (args == 2) {
        double value = lua_tonumber(L, 2);

        if (which >= 0 && which < 1048576) {
            buffer[which] = value;
            return 0;
        } else {
            lua_pushstring(L, "megablock entry out of bounds");
            lua_error(L);
            return 0;
        }
    /* Read a value from the global megabuffer. */
    } else {
        if (which >= 0 && which < 1048576) {
            lua_pushnumber(L, buffer[which]);
            return 1;
        } else {
            lua_pushstring(L, "megablock entry out of bounds");
            lua_error(L);
            return 0;
        }
    }
}

static int global_registers(lua_State *L)
{
    int args = lua_gettop(L);
    double *registers = lua_touserdata(L, lua_upvalueindex(1));
    int which = lua_tonumber(L, 1);

    /* Set a value in the global registers. */
    if (args == 2) {
        double value = lua_tonumber(L, 2);

        if (which < 0 || which > 99)
            return luaL_argerror(L, 2, "register value out of range [00-99]");

        registers[which] = value;
        return 0;
    /* Read a value from the global megabuffer. */
    } else {
        if (which < 0 || which > 99)
            return luaL_argerror(L, 2, "register value out of range [00-99]");

        lua_pushnumber(L, registers[which]);
        return 1;
    }
}

#define expr_dostring(L, string, name) \
    (luaL_loadbuffer(L, string, strlen(string), name) \
     || lua_pcall(L, 0, 0, 0))

#define expr_loadstring(L, string, name) \
    luaL_loadbuffer(L, string, strlen(string), name)

void *expr_init(AVSContext *actx)
{
    lua_State *L = luaL_newstate();
    if (L) {
        luaL_openlibs(L);

        /* Add the constants that need to be available.  Could be done in the
         * shim later but let's use the C API just to see how its done. */

        /* First get the global math table */
        lua_getglobal(L, "math");
        /* Push e (Euler's number) onto the stack. */
        lua_pushnumber(L, M_EULER);
        /* Set the field "e" of the second from top value to the top value. */
        lua_setfield(L, -2, "e");
        /* Repeat for Phi (golden ratio). */
        lua_pushnumber(L, M_PHI);
        lua_setfield(L, -2, "phi");
        /* Pop the math table from the stack. */
        lua_pop(L, 1);

        /* A C closure only differs from a C function in that a function has no
         * upvalues where as a closure has at least 1.  The Lua manual infact
         * states that lua_pushcfunction is a macro which calls lua_pushcclosure
         * with a last argument of 0. */

        lua_pushlightuserdata(L, &actx->avsdc.waveform);
        lua_pushcclosure(L, get_waveform, 1);
        lua_setglobal(L, "getosc");

        lua_pushlightuserdata(L, &actx->avsdc.spectrum);
        lua_pushcclosure(L, get_spectrum, 1);
        lua_setglobal(L, "getspec");

        lua_pushlightuserdata(L, actx->expr_global_megabuf);
        lua_pushcclosure(L, global_megabuf, 1);
        lua_setglobal(L, "__avs_global_megabuf_function");

        lua_pushlightuserdata(L, actx->expr_global_registers);
        lua_pushcclosure(L, global_registers, 1);
        lua_setglobal(L, "__avs_global_registers_function");

        if (expr_dostring(L, lua_function_helper, "lua_function_helper")) {
            ERROR("%s\n", lua_tostring(L, -1));
            lua_close(L);
            return NULL;
        }

        /* Load this shim to save us from having to replace each and every
         * instance of the old Nullsoft functions with Lua's.  It now also does
         * the various metatable shenanigans previously made with several more
         * calls to dostring. */
        if (expr_dostring(L, lua_global_helper, "lua_global_helper")) {
            ERROR("%s\n", lua_tostring(L, -1));
            lua_close(L);
            return NULL;
        }
    }

    return L;
}

void expr_uninit(void *expr_ctx)
{
    lua_State *L = expr_ctx;
    if (L)
        lua_close(L);
}

int expr_load_rstring(void *expr_ctx, const char **expr, const uint8_t *buf, int buf_len)
{
    int pos = 0;
    int size;

    R32(size);
    if (size >= 1048576 || pos + size > buf_len) {
        ERROR("string too long (%d)\n", size);
        return -1;
    }
    if (!size)
        return pos;

    if (buf[pos + size - 1] != 0) {
        ERROR("malformed expression string\n");
        return -1;
    }

    *expr = expr_sanitize(expr_ctx, (const char *)buf + pos);
    if (!expr)
        return -1;

    return pos + size;
}

const char *expr_sanitize(void *expr_ctx, const char *expr)
{
    lua_State *L = expr_ctx;

    if (!L || !expr)
        return NULL;

    /* Load the helper as a function. */
    if (expr_loadstring(L, lua_sanitize_helper, "lua_sanitize_helper")) {
        ERROR("%s\n", lua_tostring(L, -1));
        return NULL;
    }

    /* Push the expression as the argument. */
    lua_pushstring(L, expr);

    /* Call the function with 1 argument and 1 result. */
    if (lua_pcall(L, 1, 1, 0)) {
        ERROR("%s\n", lua_tostring(L, -1));
        return NULL;
    }

    /* lua_tolstring does not include the terminator in the length. */
    size_t len;
    const char *temp = lua_tolstring(L, 1, &len);
    if (!temp || len <= 0)
        return NULL;

    char *ret = malloc(len+1);
    if (!ret)
        return NULL;

    memcpy(ret, temp, len);
    ret[len] = 0;

    /* Pop the expression. */
    lua_pop(L, 1);

    return ret;
}

void expr_variable_set(void *expr_ctx, const char *name, double value)
{
    lua_State *L = expr_ctx;

    lua_pushnumber(L, value);
    lua_setglobal(L, name);
}

double expr_variable_get(void *expr_ctx, const char *name)
{
    lua_State *L = expr_ctx;

    lua_getglobal(L, name);
    double ret =  lua_tonumber(L, -1);
    lua_pop(L, 1); /* Don't forget to pop the variable! */
    return ret;
}

int expr_compile(void *expr_ctx, const char *name, const char *expr)
{
    return expr_compile2(expr_ctx, name, 0, expr, 0);
}

int expr_compile2(void *expr_ctx, const char *name, const char *args,
        const char *function_body, const char *returns)
{
    lua_State *L = expr_ctx;

    if (!function_body)
        return 0;

    luaL_Buffer b, *B = &b;
    luaL_buffinit(L, B);
    if (args)
        luaL_addstring(B, args);
    luaL_addstring(B, function_body);
    if (returns)
        luaL_addstring(B, returns);
    luaL_pushresult(B);

    const char *str = lua_tostring(L, -1);
    if (expr_loadstring(L, str, name)) {
        ERROR("%s\n%s = %s\n", lua_tostring(L, -1), name, str);
        return -1;
    }

    lua_setglobal(L, name);
    lua_pop(L, 1); /* Pop the string that was pushed from the luaL_Buffer. */

    return 0;
}

int expr_execute(void *expr_ctx, const char *name, const char *expr)
{
    lua_State *L = expr_ctx;

    if (!expr)
        return 0;
    lua_getglobal(L, name);
    if (lua_pcall(L, 0, 0, 0)) {
        ERROR("%s\n%s = %s\n", lua_tostring(L, -1), name, expr);
        return -1;
    }
    return 0;
}

int expr_execute2(void *expr_ctx, const char *name, int num_args, ...)
{
    lua_State *L = expr_ctx;

    lua_getglobal(L, name);

    va_list list;
    va_start(list, num_args);
    for (int i = 0; i < num_args; i++)
        lua_pushnumber(L, va_arg(list, double));
    va_end(list);

    if (lua_pcall(L, num_args, LUA_MULTRET, 0)) {
        ERROR("%s\n", lua_tostring(L, -1));
        return -1;
    }

    return 0;
}

void expr_clear_return(void *expr_ctx)
{
    lua_State *L = expr_ctx;
    lua_settop(L, 0);
}
