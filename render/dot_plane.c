/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "matrix.h"
#include "pixel.h"

#define NUM_WIDTH 64

struct Colour {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

typedef struct {
    int rotation_speed;
    int angle;
    int colour[5];

    /* "protected" */
    float a_table[NUM_WIDTH * NUM_WIDTH];
    float v_table[NUM_WIDTH * NUM_WIDTH];
    int c_table[NUM_WIDTH * NUM_WIDTH];
    int colour_tab[64];
    float r;
} DotPlaneContext;

static void init_colour_table(DotPlaneContext *dpctx)
{

    for (int t = 0; t < 4; t++) {
        int c0 = dpctx->colour[t];
        int c1 = dpctx->colour[t + 1];

        int r = CR_TO_I(c0) << 16;
        int g = CG_TO_I(c0) << 16;
        int b = CB_TO_I(c0) << 16;

        int dr = ((CR_TO_I(c1) - CR_TO_I(c0)) << 16) / 16;
        int dg = ((CG_TO_I(c1) - CG_TO_I(c0)) << 16) / 16;
        int db = ((CB_TO_I(c1) - CB_TO_I(c0)) << 16) / 16;

        for (int x = 0; x < 16; x++) {
            dpctx->colour_tab[t * 16 + x] = I3_TO_I(r >> 16, g >> 16, b >> 16);
            r += dr;
            g += dg;
            b += db;
        }
    }
}

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    DotPlaneContext *dpctx = ctx->priv;
    int pos = 0;

    R32(dpctx->rotation_speed);

    for (int i = 0; i < 5; i++)
        R32(dpctx->colour[i]);

    R32(dpctx->angle);

    /* Some float value stored with 27.5 fixed point precision.  That is the
     * float value is multiplied by 32.0 before being truncated to an int then
     * written to the file. */

    init_colour_table(dpctx);

    /* The original code was hard-coded to use the left spectrum.  I wonder why
     * this was done.  I changed it to the center as that seems like it should
     * be the natural choice. */
    ctx->which_vis_type = VIS_SPECTRUM;
    ctx->which_channel = VIS_CENTER;

    return 0;
}

static void print_config(ComponentContext *ctx, unused int indent)
{
    DotPlaneContext *dp = ctx->priv;

    fprintf(stderr, "    rotation_speed = %d\n", dp->rotation_speed);
    fprintf(stderr, "    angle = %d\n", dp->angle);
    for (int i = 0; i < 5; i++)
        fprintf(stderr, "    colour[%d] = #%06x\n", i, dp->colour[i]);

    return;
}

static int render(ComponentContext *ctx, uint8_t vis_data[576], int *frame, unused int *frame_out, int width, int height, unused int is_beat)
{
    DotPlaneContext *dpctx = ctx->priv;
    float b_table[NUM_WIDTH], matrix1[16], matrix2[16];
    float *a_table = dpctx->a_table;
    float *v_table = dpctx->v_table;
    int *c_table = dpctx->c_table;
    int *colour_tab = dpctx->colour_tab;
    float r = dpctx->r;

    matrix_rotate(matrix1, 2, r);
    matrix_rotate(matrix2, 1, dpctx->angle);
    matrix_multiply(matrix1, matrix2);
    matrix_translate(matrix2, 0.0, -20.0, 400.0);
    matrix_multiply(matrix1, matrix2);

    memcpy(b_table, a_table, sizeof(float) * NUM_WIDTH);

    for (int fo = 0; fo < NUM_WIDTH; fo++) {
        float *i, *o, *v, *ov;
        int *c, *oc;
        int t = (NUM_WIDTH - (fo + 2)) * NUM_WIDTH;

        i  = &a_table[t];
        o  = &a_table[t + NUM_WIDTH];
        v  = &v_table[t];
        ov = &v_table[t + NUM_WIDTH];
        c  = &c_table[t];
        oc = &c_table[t + NUM_WIDTH];

        if (fo == NUM_WIDTH - 1) {
            i = b_table;

            for (int p = 0; p < NUM_WIDTH; p++) {
                /* Damn shadowed variable! */
                /* register */ int t2 = MAX3(vis_data[0], vis_data[1], vis_data[2]);
                *o = t2;
                t2 >>= 2;

                /* Impossible */
                //if (t2 > 63)
                //t2 = 63;

                *oc++ = colour_tab[t2];
                *ov++ = (*o++ - *i++) / 90.0;
                vis_data += 3;
            }
        } else {
            for (int p = 0; p < NUM_WIDTH; p++) {
                *o = *i++ + *v;

                if (*o < 0.0)
                    *o = 0.0;

                *ov++ = *v++ - 0.15 * (*o++ / 255.0);
                *oc++ = *c++;
            }
        }
    }

    float adj = width * 440.0 / 640.0;
    float adj2 = height * 440.0 / 480.0;

    /* Changed a greater-than for a less-than and swapped the order. */
    if (adj > adj2)
        adj = adj2;

    for (int fo = 0; fo < NUM_WIDTH; fo++) {
        int f = (r < 90.0 || r > 270.0) ? NUM_WIDTH - fo - 1 : fo;
        float dw = 350.0 / NUM_WIDTH;
        float w = -(NUM_WIDTH * 0.5) * dw;
        float q = (f - NUM_WIDTH * 0.5) * dw;
        int *ct = &c_table[f * NUM_WIDTH];
        float *at = &a_table[f * NUM_WIDTH];
        int da = 1;

        if (r < 180.0) {
            da = -1;
            dw = -dw;
            w = -w + dw;
            ct += NUM_WIDTH - 1;
            at += NUM_WIDTH - 1;
        }

        for (int p = 0; p < NUM_WIDTH; p++) {
            float x, y, z;

            matrix_apply(matrix1, w, 64.0 - *at, q, &x, &y, &z);
            z = adj / z;
            /* register */ int ix = (x * z) + (width / 2);
            /* register */ int iy = (y * z) + (height / 2);

            if (iy >= 0 && iy < height && ix >= 0 && ix < width)
                blend_pixel(frame + iy * width + ix, *ct, ctx->blend_mode);

            w += dw;
            ct += da;
            at += da;
        }
    }

    r += dpctx->rotation_speed / 5.0;
    if (r >= 360.0)
        r -= 360.0;
    if (r < 0.0)
        r += 360.0;
    dpctx->r = r;

    return 0;
}

Component r_dotplane = {
    .name = "Render / Dot Plane",
    .code = 1,
    .priv_size = sizeof(DotPlaneContext),
    .load_config = load_config,
    .print_config = print_config,
    .render = render,
};
