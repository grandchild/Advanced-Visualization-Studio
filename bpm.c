/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "avs.h"
#include "bpm.h"
#include "common.h"

#define HISTORY_SIZE 8

#define BEAT_REAL             1
#define BEAT_GUESSED          2

#define MAX_BPM             170
#define MIN_BPM              60

#define BETTER_CONF_ADOPT     2
#define TOP_CONF_ADOPT        8
#define MIN_STICKY            8
#define STICKY_THRESHOLD     70
#define STICKY_THRESHOLD_LOW 85

typedef struct {
    uint32_t TC;
    int Type;
} BeatType;

struct BeatContext {
    int beat_peak1, beat_peak2, beat_count, beat_peak1_peak;

    int cfg_smart_beat;
    int cfg_smart_beat_sticky;
    int cfg_smart_beat_reset_new_song;
    int cfg_smart_beat_only_sticky;
    int sticked;
    /* Values of arbitrary beat and beat skip. */
    int arb_val, skip_val;
    /* Calculated BPM (realtime), Confidence computation. */
    int bpm, confidence, confidence1, confidence2;
    uint32_t last_TC;
    uint32_t last_TC2;
    BeatType TC_hist[8];
    BeatType TC_hist2[8];
    int smoother[8];
    int half_discriminated[8];
    int half_discriminated2[8];
    int hd_pos;
    int hd_pos2;
    int smPtr, smSize;
    int TCHistPtr;
    //int TCHistSize; /* Removed in favour of a pre-processor define. */
    int offIMax;
    int last_bpm;
    int insertion_count;
    uint32_t prediction_last_TC;
    uint32_t avg;
    uint32_t avg2;
    int skip_count;
    int in_inc, out_inc;
    int in_slide, out_slide;
    int old_in_slide, old_out_slide;
    int old_sticked;
    // char txt[256];
    int half_count, double_count;
    int TC_used;
    int prediction_bpm;
    int old_display_bpm, old_display_confidence;
    int best_confidence;
    // char lastsongname[256];
    // HWND winampWND;
    int force_new_beat;
    int better_confidence_count;
    int top_confidence_count;
    int sticky_confidence_count;
    int do_resync_bpm;
};

static void double_beat(BeatContext *);
static void half_beat(BeatContext *);
static int ready_to_learn(BeatContext *);
static int ready_to_guess(BeatContext *);

void *init_bpm(enum BeatConfig config)
{
    BeatContext *ctx = calloc(1, sizeof(BeatContext));
    if (!ctx)
        return ctx;

    ctx->cfg_smart_beat_sticky = 1;
    ctx->cfg_smart_beat_reset_new_song = 1;
    ctx->do_resync_bpm = 0;
    ctx->old_display_bpm = -1;
    ctx->old_display_confidence = -1;
    ctx->smSize = 8;
    ctx->offIMax = 8;
    //ctx->TCHistSize = 8; /* Removed in favour of a pre-processor define. */
    ctx->old_sticked = -1;

    if (config & BEAT_ADVANCED)
        ctx->cfg_smart_beat = 1;

    return ctx;
}

int bpm_frame_has_beat(BeatContext *ctx, AVSDataContext *avsdc)
{
    int beat = 0, lt[2] = {0, 0};

    for (int i = 0; i < 576; i++) {
        lt[0] += abs(avsdc->waveform.left[i] - 128);
        lt[1] += abs(avsdc->waveform.right[i] - 128);
    }
    int max_sum = MAX(lt[0], lt[1]);

    ctx->beat_peak1 = (ctx->beat_peak1 * 125 + ctx->beat_peak2 * 3) / 128;
    ctx->beat_count++;
    if (lt[0] >= (ctx->beat_peak1 * 34) / 32 && max_sum > 576 * 16) {
        if (ctx->beat_count > 0) {
            ctx->beat_count = 0;
            beat = 1;
        }
        ctx->beat_peak1 = (max_sum + ctx->beat_peak1_peak) / 2;
        ctx->beat_peak1_peak = max_sum;
    } else if (max_sum > ctx->beat_peak2)
        ctx->beat_peak2 = max_sum;
    else
        ctx->beat_peak2 = (ctx->beat_peak2 * 14) / 16;

    return beat;
}

static void reset_adapt(BeatContext *ctx, uint32_t TCNow)
{
    memset(ctx, 0, sizeof(BeatContext));
    ctx->offIMax = 8;
    //ctx->TCHistSize = 8; /* Removed in favour of a pre-processor define. */
    ctx->last_TC = TCNow;
    ctx->old_sticked = -1;
}

/* Insert a beat in history table.  May be either real or guessed. */
static void insert_hist_step(BeatContext *ctx, BeatType *t, uint32_t TC, int Type, int i)
{
    if (i >= HISTORY_SIZE)
        return;
    if (t == ctx->TC_hist && ctx->insertion_count < HISTORY_SIZE * 2)
        ctx->insertion_count++;
    memmove(t + i + 1, t + 1, sizeof(BeatType) * (HISTORY_SIZE - (i + 1)));
    t[0].TC = TC;
    t[0].Type = Type;
}

static void double_beat(BeatContext *ctx)
{
    int iv[8];

    if (ctx->sticked && ctx->bpm > MIN_BPM)
        return;

    for (int i = 0; i < HISTORY_SIZE - 1; i++)
        iv[i] = ctx->TC_hist[i].TC - ctx->TC_hist[i + 1].TC;

    for (int i = 1; i < HISTORY_SIZE; i++)
        ctx->TC_hist[i].TC = ctx->TC_hist[i - 1].TC - iv[i - 1] / 2;

    ctx->avg /= 2;
    ctx->bpm *= 2;
    ctx->double_count = 0;
    memset(ctx->smoother, 0, ctx->smSize * sizeof(int));
    memset(ctx->half_discriminated, 0, HISTORY_SIZE * sizeof(int));
}

static void half_beat(BeatContext *ctx)
{
    int iv[8];

    if (ctx->sticked && ctx->bpm < MIN_BPM)
        return;

    for (int i = 0; i < HISTORY_SIZE - 1; i++)
        iv[i] = ctx->TC_hist[i].TC - ctx->TC_hist[i + 1].TC;

    for (int i = 1; i < HISTORY_SIZE; i++)
        ctx->TC_hist[i].TC = ctx->TC_hist[i - 1].TC - iv[i - 1] * 2;

    ctx->avg *= 2;
    ctx->bpm /= 2;
    ctx->half_count = 0;
    memset(ctx->smoother, 0, ctx->smSize * sizeof(int));
    memset(ctx->half_discriminated, 0, HISTORY_SIZE * sizeof(int));
}

static int TC_hist_step(BeatContext *ctx, uint32_t TCNow, int Type)
{
    int offI;
    uint32_t this_len;
    int learning = ready_to_learn(ctx);
    this_len = TCNow - ctx->last_TC;

    /* If this beat is sooner then half the average - 20%, throw it away. */
    if (this_len < ctx->avg / 2 - ctx->avg * 0.2) {
        if (learning) {
            if (fabs(ctx->avg - (TCNow - ctx->TC_hist[1].TC)) < fabs(ctx->avg - (ctx->TC_hist[0].TC - ctx->TC_hist[1].TC))) {
#if 0
                if (ctx->prediction_last_TC && ctx->TC_hist[0].Type == BEAT_GUESSED && type == BEAT_REAL)
                    ctx->prediction_last_TC += (TCNow - ctx->TC_hist[0].TC) / 2;
#endif
                ctx->TC_hist[0].TC = TCNow;
                ctx->TC_hist[0].Type = Type;
                return 1;
            }
        }
        return 0;
    }

    if (learning) {
        /* Try to see if this beat is in the middle of out current BPM, or maybe
         * 1/3, 1/4, etc... to offIMax. */
        for (offI = 2; offI < ctx->offIMax; offI++) {
            if (fabs((ctx->avg / offI) - this_len) < (ctx->avg / offI) * 0.2) {
                /* Shoudl test that offI == 2 before doing that, but it seems to
                 * have better results?  I'll have to investigate this. */
                ctx->half_discriminated[ctx->hd_pos++] = 0;
                ctx->hd_pos %= 8;
                return 0;
            }
        }
    }

    ctx->half_discriminated[ctx->hd_pos++] = 0;
    ctx->hd_pos %= 8;

    /* Check if we missed some beats. */
#if 0
    if (learning && this_len > 1000 || abs(ctx->avg - this_len) > ctx->avg * 0.3) {
        for (offI = 2; offI < ctx->offIMax; offI++) {
            if(abs((ctx->avg * offI) - this_len) < ctx->avg * 0.2) {
                /* Oh yeah, we definitely did, add one! */
                for (int i = 1; i < offI; i++) {
                    /* Beat has been guessed so report it so confidence can be
                     * calculated. */
                    insert_hist_step(TCNow - (ctx->avg * i), BEAT_GUESSED, offI - 1);
                }
                break;
            }
        }
    }
#endif

    /* Remember this tick count. */
    ctx->last_TC = TCNow;
    /* Insert this beat. */
    insert_hist_step(ctx, ctx->TC_hist, TCNow, Type, 0);
    return 1;
}

/* Am I ready to learn? */
static int ready_to_learn(BeatContext *ctx)
{
    for (int i = 0; i < HISTORY_SIZE; i++)
        if (ctx->TC_hist[i].TC == 0)
            return 0;
    return 1;
}

/* Am I ready to guess? */
static int ready_to_guess(BeatContext *ctx)
{
    return ctx->insertion_count == HISTORY_SIZE * 2;
}

static void new_bpm(BeatContext *ctx, int this_bpm)
{
    ctx->smoother[ctx->smPtr++] = this_bpm;
    ctx->smPtr %= ctx->smSize;
}

/* Calculate BPM according to beat history. */
static int get_bpm(BeatContext *ctx)
{
    int smN = 0;
    int smSum = 0;

    /* Calculate smoothed BPM. */
    for (int i = 0; i < ctx->smSize; i++) {
        if (ctx->smoother[i] > 0) {
            smSum += ctx->smoother[i];
            smN++;
        }
    }

    if (smN)
        return smSum / smN;

    return 0;
}

static void calc_bpm(BeatContext *ctx, uint32_t TCNow)
{
    int hd_count = 0;
    int r = 0;
    int total_TC = 0;
    int totalN = 0;
    float rC, etC;
    int v;
    float sc = 0;
    int mx = 0;
    float et;
    //int smSum = 0;
    //int smN = 0;

    if (!ready_to_learn(ctx))
        return;

    /* First calculate average beat. */
    for (int i = 0; i < HISTORY_SIZE - 1; i++)
        total_TC += ctx->TC_hist[i].TC - ctx->TC_hist[i + 1].TC;

    ctx->avg = total_TC / (HISTORY_SIZE - 1);

    /* Calculate how many of them are real opposed to guessed. */
    for (int i = 0; i < HISTORY_SIZE; i++)
        if (ctx->TC_hist[i].Type == BEAT_REAL)
            r++;

    /* Calculate part 1 of confidence. */
    rC = MIN((r / (float)HISTORY_SIZE) * 2, 1);

    /* Calculate typical drift. */
    for (int i = 0; i < HISTORY_SIZE - 1; i++) {
        v = ctx->TC_hist[i].TC - ctx->TC_hist[i + 1].TC;
        mx = MAX(mx, v);
        sc += v * v;
    }
    et = sqrtf(sc / (float)(HISTORY_SIZE-1) - ctx->avg * ctx->avg);

    /* Calculate confidence based on typical drift and max derivation. */
    etC = 1 - (et / (float)mx);

    /* Calculate confidence. */
    ctx->confidence = MAX(0, (rC * etC * 100.0 - 50) * 2);
    ctx->confidence1 = rC * 100;
    ctx->confidence2 = etC * 100;

    /* Now apply second layer, recalulate average using only beats within range
     * of typical drift.  Also, count how many of them we are keeping. */
    total_TC = 0;
    for (int i = 0; i < HISTORY_SIZE - 1; i++) {
        v += ctx->TC_hist[i].TC - ctx->TC_hist[i + 1].TC;
        if (fabs(ctx->avg - v) < et) {
            total_TC += v;
            totalN++;
            v = 0;
        } else
            if (v > ctx->avg)
                v = 0;
    }

    ctx->TC_used = totalN;

    /* If no beat was within typical drift (How would it be possible?  Lets
     * cover it anyway.) then keep the simple average calculated earlier, else
     * recalculate average of beats within range. */
    if (totalN)
        ctx->avg = total_TC / totalN;

    if (ready_to_guess(ctx)) {
        /* avg = 0?  Ahem... */
        if (ctx->avg)
            ctx->bpm = 60000 / ctx->avg;

        if (ctx->bpm != ctx->last_bpm) {
            /* If realtime BPM has changed since last time, then insert it in
             * the smoothing table. */
            new_bpm(ctx, ctx->bpm);
            ctx->last_bpm = ctx->bpm;

            if (ctx->cfg_smart_beat_sticky && ctx->prediction_bpm
                    && ctx->confidence >= ((ctx->prediction_bpm < 90) ? STICKY_THRESHOLD_LOW : STICKY_THRESHOLD)) {
                ctx->sticky_confidence_count++;
                if(ctx->sticky_confidence_count >= MIN_STICKY)
                    ctx->sticked = 1;
            } else
                ctx->sticky_confidence_count = 0;
        }

        get_bpm(ctx);

        /* Count how many beats we discriminated. */
        for (int i = 0; i < HISTORY_SIZE; i++)
            if (ctx->half_discriminated[i])
                hd_count++;

        /* If we removed at least half of our beats, then we are off course.  We
         * should double our BPM. */
        if (hd_count >= HISTORY_SIZE / 2) {
            /* Lets do so only if the doubled BPM is less than BPM_MAX. */
            if (ctx->bpm * 2 < MAX_BPM) {
                double_beat(ctx);
                /* Reset discrimination table. */
                memset(ctx->half_discriminated, 0, HISTORY_SIZE * sizeof(int));
            }
        }

        if (ctx->bpm > 500 || ctx->bpm < 0)
            reset_adapt(ctx, TCNow);

        /* We're going too slow, lets double our BPM. */
        if (ctx->bpm < MIN_BPM) {
            if (++ctx->double_count > 4)
                double_beat(ctx);
        } else
            ctx->double_count = 0;

        /* We're going too fast, lets halve our BPM. */
        if (ctx->bpm > MAX_BPM) {
            if (++ctx->half_count > 4)
                half_beat(ctx);
        } else
            ctx->half_count = 0;
    }
}

int refine_beat(BeatContext *ctx, int is_beat, uint32_t TCNow)
{
    int accepted = 0;
    int predicted = 0;
    int resync_in = 0;
    int resync_out = 0;

#if 0
    if (song_changed(TCNow)) {
        best_confidence = best_confidence * 0.5;
        sticked = 0;
        sticky_confidence_count = 0;
        if (cfg_smart_beat_reset_new_song)
            reset_adapt();
    }
#endif

    if (ctx->bpm && TCNow > ctx->prediction_last_TC + (60000 / ctx->bpm))
        predicted = 1;

    if (is_beat)
        accepted = TC_hist_step(ctx, TCNow, BEAT_REAL);

    calc_bpm(ctx, TCNow);

    // If prediction Bpm has not yet been set
    // or if prediction bpm is too high or too low
    // or if 3/4 of our history buffer contains beats within the range of typical drift
    // the accept the calculated Bpm as the new prediction Bpm
    // This allows keeping the beat going on when the music fades out, and readapt to the new beat as soon as
    // the music fades in again
    if ((accepted || predicted) && !ctx->sticked && (!ctx->prediction_bpm || ctx->prediction_bpm > MAX_BPM || ctx->prediction_bpm < MIN_BPM)) {
        if (ctx->confidence >= ctx->best_confidence) {
#if 0
            ctx->better_confidence_count++;
            if (!ctx->prediction_bpm || ctx->better_confidence_count == BETTER_CONF_ADOPT) {
#endif
                ctx->force_new_beat = 1;
#if 0
                ctx->better_confidence_count = 0;
            }
#endif
        }

        if (ctx->confidence >= 50) {
            ctx->top_confidence_count++;
            if (ctx->top_confidence_count == TOP_CONF_ADOPT) {
                ctx->force_new_beat = 1;
                ctx->top_confidence_count = 0;
            }
        }

        if (ctx->force_new_beat) {
            ctx->force_new_beat = 0;
            ctx->best_confidence = ctx->confidence;
            ctx->prediction_bpm = ctx->bpm;
        }
    }

    if (!ctx->sticked)
        ctx->prediction_bpm = ctx->bpm;

    ctx->bpm = ctx->prediction_bpm;

#if 0
    resync = ctx->prediction+bpm
            && (ctx->prediction_last_TC < TCNow - (30000 / ctx->prediction_bpm) - (60000 / ctx->precistion_bpm) * 0.2)
            || (ctx->prediction_last_TC < TCNow - (30000 / ctx->prediction_bpm) - (60000 / ctx->precistion_bpm) * 0.2);
    /* Why this duplicated line? */
#endif

    if (ctx->prediction_bpm && accepted && !predicted) {
        int b;
        if (TCNow > ctx->prediction_last_TC + (60000 / ctx->prediction_bpm) * 0.7) {
            resync_in = 1;
            b = ctx->prediction_bpm * 1.01;
        }
        if (TCNow < ctx->prediction_last_TC + (60000 / ctx->prediction_bpm) * 0.3) {
            //int b; /* This shadowed varaible must be wrong. */
            resync_out = 1;
            b = ctx->prediction_bpm * 0.98;
        }
        if (!ctx->sticked && ctx->do_resync_bpm && (resync_in || resync_out)) {
            new_bpm(ctx, b);
            ctx->prediction_bpm = get_bpm(ctx);
        }
    }

    if (resync_in) {
        ctx->prediction_last_TC = TCNow;
        ctx->do_resync_bpm = 1;
        return ((ctx->cfg_smart_beat && !ctx->cfg_smart_beat_only_sticky)
                || (ctx->cfg_smart_beat && ctx->cfg_smart_beat_only_sticky && ctx->sticked)) ? 1 : is_beat;
    }

    if (predicted) {
        ctx->prediction_last_TC = TCNow;
        if (ctx->confidence > 25)
            TC_hist_step(ctx, TCNow, BEAT_GUESSED);
        ctx->do_resync_bpm = 0;
        return ((ctx->cfg_smart_beat && !ctx->cfg_smart_beat_only_sticky)
                || (ctx->cfg_smart_beat && ctx->cfg_smart_beat_only_sticky && ctx->sticked)) ? 1 : is_beat;
    }

    if (resync_out) {
        ctx->prediction_last_TC = TCNow;
        ctx->do_resync_bpm = 1;
        return ((ctx->cfg_smart_beat && !ctx->cfg_smart_beat_only_sticky)
                || (ctx->cfg_smart_beat && ctx->cfg_smart_beat_only_sticky && ctx->sticked)) ? 0 : is_beat;
    }

    return ((ctx->cfg_smart_beat && !ctx->cfg_smart_beat_only_sticky)
            || (ctx->cfg_smart_beat && ctx->cfg_smart_beat_only_sticky && ctx->sticked)) ? (ctx->prediction_bpm ? 0 : is_beat) : is_beat;
}

