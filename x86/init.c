/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define AV_CPU_FLAG_MMX          0x0001 ///< standard MMX
#define AV_CPU_FLAG_MMXEXT       0x0002 ///< SSE integer functions or AMD MMX ext
#define AV_CPU_FLAG_MMX2         0x0002 ///< SSE integer functions or AMD MMX ext
#define AV_CPU_FLAG_3DNOW        0x0004 ///< AMD 3DNOW
#define AV_CPU_FLAG_SSE          0x0008 ///< SSE functions
#define AV_CPU_FLAG_SSE2         0x0010 ///< PIV SSE2 functions
#define AV_CPU_FLAG_SSE2SLOW 0x40000000 ///< SSE2 supported, but usually not faster
                                        ///< than regular MMX/SSE (e.g. Core1)
#define AV_CPU_FLAG_3DNOWEXT     0x0020 ///< AMD 3DNowExt
#define AV_CPU_FLAG_SSE3         0x0040 ///< Prescott SSE3 functions
#define AV_CPU_FLAG_SSE3SLOW 0x20000000 ///< SSE3 supported, but usually not faster
                                        ///< than regular MMX/SSE (e.g. Core1)
#define AV_CPU_FLAG_SSSE3        0x0080 ///< Conroe SSSE3 functions
#define AV_CPU_FLAG_ATOM     0x10000000 ///< Atom processor, some SSSE3 instructions are slower
#define AV_CPU_FLAG_SSE4         0x0100 ///< Penryn SSE4.1 functions
#define AV_CPU_FLAG_SSE42        0x0200 ///< Nehalem SSE4.2 functions
#define AV_CPU_FLAG_AVX          0x4000 ///< AVX functions: requires OS support even if YMM registers aren't used
#define AV_CPU_FLAG_AVXSLOW   0x8000000 ///< AVX supported, but slow when using YMM registers (e.g. Bulldozer)
#define AV_CPU_FLAG_XOP          0x0400 ///< Bulldozer XOP functions
#define AV_CPU_FLAG_FMA4         0x0800 ///< Bulldozer FMA4 functions
#define AV_CPU_FLAG_CMOV      0x1001000 ///< supports cmov instruction
#define AV_CPU_FLAG_AVX2         0x8000 ///< AVX2 functions: requires OS support even if YMM registers aren't used
#define AV_CPU_FLAG_FMA3        0x10000 ///< Haswell FMA3 functions
#define AV_CPU_FLAG_BMI1        0x20000 ///< Bit Manipulation Instruction Set 1
#define AV_CPU_FLAG_BMI2        0x40000 ///< Bit Manipulation Instruction Set 2

#include "block.h"

#define THREE_FUNCTION_MMX(name, ...) \
    void avsint_##name##_mmx(__VA_ARGS__);  \
    void avsint_##name##_sse2(__VA_ARGS__); \
    void avsint_##name##_avx2(__VA_ARGS__)

#define THREE_FUNCTION_MMX2(name, ...) \
    void avsint_##name##_mmx2(__VA_ARGS__); \
    void avsint_##name##_sse2(__VA_ARGS__); \
    void avsint_##name##_avx2(__VA_ARGS__)

THREE_FUNCTION_MMX2(blend_block_avg, int *,int *, int *, int);
THREE_FUNCTION_MMX(blend_block_add, int *,int *, int *, int);
THREE_FUNCTION_MMX(blend_block_sub, int *,int *, int *, int);
THREE_FUNCTION_MMX(blend_block_xor, int *,int *, int *, int);
THREE_FUNCTION_MMX2(blend_block_max, int *,int *, int *, int);
THREE_FUNCTION_MMX2(blend_block_min, int *,int *, int *, int);
THREE_FUNCTION_MMX(blend_block_mul, int *,int *, int *, int);
THREE_FUNCTION_MMX(blend_block_adj, int *,int *, int *, int, int);

THREE_FUNCTION_MMX2(blend_block_const_avg, int *,int *, int, int);
THREE_FUNCTION_MMX(blend_block_const_add, int *,int *, int, int);
THREE_FUNCTION_MMX(blend_block_const_sub1, int *,int *, int, int);
THREE_FUNCTION_MMX(blend_block_const_sub2, int *,int, int *, int);
THREE_FUNCTION_MMX(blend_block_const_xor, int *,int *, int, int);
THREE_FUNCTION_MMX2(blend_block_const_max, int *,int *, int, int);
THREE_FUNCTION_MMX2(blend_block_const_min, int *,int *, int, int);
THREE_FUNCTION_MMX(blend_block_const_mul, int *,int *, int, int);
THREE_FUNCTION_MMX(blend_block_const_adj, int *,int *, int, int, int);

/*
 * Trans / Water.
 */

extern void (*water_line)(int *, int *, int *, int);
void avsint_water_line_mmx(int *, int *, int *, int);
void avsint_water_line_sse2(int *, int *, int *, int);

/*
 * Trans / Unique Tone.
 */

extern void (*unique_tone_add)(int *, int *, int, int);
void avsint_unique_tone_add_mmx2(int *, int *, int, int);
void avsint_unique_tone_add_sse2(int *, int *, int, int);

extern void (*unique_tone_avg)(int *, int *, int, int);
void avsint_unique_tone_avg_mmx2(int *, int *, int, int);
void avsint_unique_tone_avg_sse2(int *, int *, int, int);

extern void (*unique_tone_replace)(int *, int *, int, int);
void avsint_unique_tone_replace_mmx2(int *, int *, int, int);
void avsint_unique_tone_replace_sse2(int *, int *, int, int);

extern void (*unique_tone_add_inv)(int *, int *, int, int);
void avsint_unique_tone_add_inv_mmx2(int *, int *, int, int);
void avsint_unique_tone_add_inv_sse2(int *, int *, int, int);

extern void (*unique_tone_avg_inv)(int *, int *, int, int);
void avsint_unique_tone_avg_inv_mmx2(int *, int *, int, int);
void avsint_unique_tone_avg_inv_sse2(int *, int *, int, int);

extern void (*unique_tone_replace_inv)(int *, int *, int, int);
void avsint_unique_tone_replace_inv_mmx2(int *, int *, int, int);
void avsint_unique_tone_replace_inv_sse2(int *, int *, int, int);

/*
 * Trans / Fast Brightness.
 */

extern void (*fast_brightness_mul2)(int *frame, int pixels);
THREE_FUNCTION_MMX(fast_brightness_mul2, int *, int);

extern void (*fast_brightness_div2)(int *frame, int pixels);
THREE_FUNCTION_MMX(fast_brightness_div2, int *, int);

/*
 * Trans / Channel Shift.
 */

extern void (*channel_shift)(int *, int, int);
void avsint_channel_shift_ssse3(int *, int, int);
void avsint_channel_shift_avx2(int *, int, int);

/*
 * Trans / Colour Reduction.
 */

extern void (*colour_reduction)(int *, int, int);
THREE_FUNCTION_MMX(colour_reduction, int *, int, int);

/*
 * Trans / Multiplier.
 */

extern void (*multiplier_mul1)(int *, int);
THREE_FUNCTION_MMX(multiplier_mul1, int *, int);

extern void (*multiplier_mul2)(int *, int);
THREE_FUNCTION_MMX(multiplier_mul2, int *, int);

extern void (*multiplier_mul3)(int *, int);
THREE_FUNCTION_MMX(multiplier_mul3, int *, int);

extern void (*multiplier_div1)(int *, int);
THREE_FUNCTION_MMX(multiplier_div1, int *, int);

extern void (*multiplier_div2)(int *, int);
THREE_FUNCTION_MMX(multiplier_div2, int *, int);

extern void (*multiplier_div3)(int *, int);
THREE_FUNCTION_MMX(multiplier_div3, int *, int);

extern void (*multiplier_infrt)(int *, int);
THREE_FUNCTION_MMX(multiplier_infrt, int *, int);

extern void (*multiplier_infsq)(int *, int);
THREE_FUNCTION_MMX(multiplier_infsq, int *, int);

void init_x86(int cpu_flags)
{

#define ASSIGN(name, simd) \
    name = avsint_##name##_##simd

    if(cpu_flags & AV_CPU_FLAG_MMX) {
        blend_block_add = avsint_blend_block_add_mmx;
        blend_block_sub = avsint_blend_block_sub_mmx;
        blend_block_xor = avsint_blend_block_xor_mmx;
        blend_block_mul = avsint_blend_block_mul_mmx;
        blend_block_adj = avsint_blend_block_adj_mmx;

        blend_block_const_add = avsint_blend_block_const_add_mmx;
        blend_block_const_sub1 = avsint_blend_block_const_sub1_mmx;
        blend_block_const_sub2 = avsint_blend_block_const_sub2_mmx;
        blend_block_const_xor = avsint_blend_block_const_xor_mmx;
        blend_block_const_mul = avsint_blend_block_const_mul_mmx;
        blend_block_const_adj = avsint_blend_block_const_adj_mmx;

        water_line = avsint_water_line_mmx;

        fast_brightness_mul2 = avsint_fast_brightness_mul2_mmx;
        fast_brightness_div2 = avsint_fast_brightness_div2_mmx;

        colour_reduction = avsint_colour_reduction_mmx;

        ASSIGN(multiplier_mul1, mmx);
        ASSIGN(multiplier_mul2, mmx);
        ASSIGN(multiplier_mul3, mmx);
        ASSIGN(multiplier_div1, mmx);
        ASSIGN(multiplier_div2, mmx);
        ASSIGN(multiplier_div3, mmx);
        ASSIGN(multiplier_infrt, mmx);
        ASSIGN(multiplier_infsq, mmx);
    }

    if(cpu_flags & AV_CPU_FLAG_MMX2) {
        blend_block_avg = avsint_blend_block_avg_mmx2;
        blend_block_max = avsint_blend_block_max_mmx2;
        blend_block_min = avsint_blend_block_min_mmx2;

        blend_block_const_avg = avsint_blend_block_const_avg_mmx2;
        blend_block_const_max = avsint_blend_block_const_max_mmx2;
        blend_block_const_min = avsint_blend_block_const_min_mmx2;

        unique_tone_add = avsint_unique_tone_add_mmx2;
        unique_tone_avg = avsint_unique_tone_avg_mmx2;
        unique_tone_replace = avsint_unique_tone_replace_mmx2;

        unique_tone_add_inv = avsint_unique_tone_add_inv_mmx2;
        unique_tone_avg_inv = avsint_unique_tone_avg_inv_mmx2;
        unique_tone_replace_inv = avsint_unique_tone_replace_inv_mmx2;
    }

    if (cpu_flags & AV_CPU_FLAG_SSE2) {
        blend_block_avg = avsint_blend_block_avg_sse2;
        blend_block_add = avsint_blend_block_add_sse2;
        blend_block_sub = avsint_blend_block_sub_sse2;
        blend_block_xor = avsint_blend_block_xor_sse2;
        blend_block_max = avsint_blend_block_max_sse2;
        blend_block_min = avsint_blend_block_min_sse2;
        blend_block_mul = avsint_blend_block_mul_sse2;
        blend_block_adj = avsint_blend_block_adj_sse2;

        blend_block_const_avg = avsint_blend_block_const_avg_sse2;
        blend_block_const_add = avsint_blend_block_const_add_sse2;
        blend_block_const_sub1 = avsint_blend_block_const_sub1_sse2;
        blend_block_const_sub2 = avsint_blend_block_const_sub2_sse2;
        blend_block_const_xor = avsint_blend_block_const_xor_sse2;
        blend_block_const_max = avsint_blend_block_const_max_sse2;
        blend_block_const_min = avsint_blend_block_const_min_sse2;
        blend_block_const_mul = avsint_blend_block_const_mul_sse2;
        blend_block_const_adj = avsint_blend_block_const_adj_sse2;

        water_line = avsint_water_line_sse2;

        unique_tone_add = avsint_unique_tone_add_sse2;
        unique_tone_avg = avsint_unique_tone_avg_sse2;
        unique_tone_replace = avsint_unique_tone_replace_sse2;

        unique_tone_add_inv = avsint_unique_tone_add_inv_sse2;
        unique_tone_avg_inv = avsint_unique_tone_avg_inv_sse2;
        unique_tone_replace_inv = avsint_unique_tone_replace_inv_sse2;

        fast_brightness_mul2 = avsint_fast_brightness_mul2_sse2;
        fast_brightness_div2 = avsint_fast_brightness_div2_sse2;

        colour_reduction = avsint_colour_reduction_sse2;

        ASSIGN(multiplier_mul1, sse2);
        ASSIGN(multiplier_mul2, sse2);
        ASSIGN(multiplier_mul3, sse2);
        ASSIGN(multiplier_div1, sse2);
        ASSIGN(multiplier_div2, sse2);
        ASSIGN(multiplier_div3, sse2);
        ASSIGN(multiplier_infrt, sse2);
        ASSIGN(multiplier_infsq, sse2);
    }

    if (cpu_flags & AV_CPU_FLAG_SSSE3) {
        channel_shift = avsint_channel_shift_ssse3;
    }

    if (cpu_flags & AV_CPU_FLAG_AVX2) {
        blend_block_avg = avsint_blend_block_avg_avx2;
        blend_block_add = avsint_blend_block_add_avx2;
        blend_block_sub = avsint_blend_block_sub_avx2;
        blend_block_xor = avsint_blend_block_xor_avx2;
        blend_block_max = avsint_blend_block_max_avx2;
        blend_block_min = avsint_blend_block_min_avx2;
        blend_block_mul = avsint_blend_block_mul_avx2;
        blend_block_adj = avsint_blend_block_adj_avx2;

        blend_block_const_avg = avsint_blend_block_const_avg_avx2;
        blend_block_const_add = avsint_blend_block_const_add_avx2;
        blend_block_const_sub1 = avsint_blend_block_const_sub1_avx2;
        blend_block_const_sub2 = avsint_blend_block_const_sub2_avx2;
        blend_block_const_xor = avsint_blend_block_const_xor_avx2;
        blend_block_const_max = avsint_blend_block_const_max_avx2;
        blend_block_const_min = avsint_blend_block_const_min_avx2;
        blend_block_const_mul = avsint_blend_block_const_mul_avx2;
        blend_block_const_adj = avsint_blend_block_const_adj_avx2;

        fast_brightness_mul2 = avsint_fast_brightness_mul2_avx2;
        fast_brightness_div2 = avsint_fast_brightness_div2_avx2;

        colour_reduction = avsint_colour_reduction_avx2;

        channel_shift = avsint_channel_shift_avx2;

        ASSIGN(multiplier_mul1, avx2);
        ASSIGN(multiplier_mul2, avx2);
        ASSIGN(multiplier_mul3, avx2);
        ASSIGN(multiplier_div1, avx2);
        ASSIGN(multiplier_div2, avx2);
        ASSIGN(multiplier_div3, avx2);
        ASSIGN(multiplier_infrt, avx2);
        ASSIGN(multiplier_infsq, avx2);
    }
}
