;*
;* Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
;*
;* This File is part of Advanced Visualization Studio.
;*
;* This program is free software: you can redistribute it and/or modify
;* it under the terms of the GNU General Public License as published by
;* the Free Software Foundation, either version 3 of the License, or
;* (at your option) any later version.
;*
;* This program is distributed in the hope that it will be useful,
;* but WITHOUT ANY WARRANTY; without even the implied warranty of
;* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;* GNU General Public License for more details.
;*
;* You should have received a copy of the GNU General Public License
;* along with this program.  If not, see <http://www.gnu.org/licenses/>.
;*

%include "x86/x86util.asm"
%include "x86/constants.inc"

SECTION_RODATA

pd_255: times  8 dd 255

SECTION_TEXT

%macro emms_if_mmx 0
    %if mmsize == 8
        emms
    %endif
%endmacro

%macro UNIQUE_TONE_INTERNAL 3
cglobal unique_tone_%1, 3, 3, 4, 0, dst, src, pixels, colour
    movd       m0,  colourm
    SPLATD     m0
    punpcklbw  m0, [pb_0]

    align 16
    .loop:
        movh   m1, [srcq]
        movh   m2, [srcq+1]
        movh   m3, [srcq+2]
        pmaxub m1,  m2
        pmaxub m1,  m3
        pand   m1, [pd_255] ; max of rgb is in the low byte of each dword

        %if %3
            mova   m2, [pd_255]
            psubd  m2,  m1
            SWAP   1,   2
        %endif

        %if mmsize == 8
            pshufw    m1,  m1, q0000
        %else
            pslldq    m2,  m1, 1
            pslldq    m3,  m1, 2
            por       m1,  m2
            por       m1,  m3 ; max in the lower 3 bytes of each dword
            punpcklbw m1, [pb_0]
        %endif

        pmullw    m1, m0
        psrlw     m1, 8 ; (max * colour) / 256
        packuswb  m1, m1

        %ifidn %2,add
            movh    m2, [srcq]
            paddusb m1,  m2
        %elifidn %2,avg
            movh    m2, [srcq]
            pavgb   m1,  m2
        %endif

        movh [dstq],   m1

        add   dstq,    mmsize/2
        add   srcq,    mmsize/2
        sub   pixelsd, mmsize/8
    jg .loop

    emms_if_mmx
RET
%endmacro

%macro UNIQUE_TONE 1
    UNIQUE_TONE_INTERNAL %1, %1, 0
%endmacro

%macro UNIQUE_TONE_INV 1
    UNIQUE_TONE_INTERNAL %1_inv, %1, 1
%endmacro

INIT_MMX mmx2
UNIQUE_TONE add
UNIQUE_TONE avg
UNIQUE_TONE replace
UNIQUE_TONE_INV add
UNIQUE_TONE_INV avg
UNIQUE_TONE_INV replace


INIT_XMM sse2
UNIQUE_TONE add
UNIQUE_TONE avg
UNIQUE_TONE replace
UNIQUE_TONE_INV add
UNIQUE_TONE_INV avg
UNIQUE_TONE_INV replace
