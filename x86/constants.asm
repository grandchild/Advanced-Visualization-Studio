;*
;* Copyright (c) 2015 James Darnley <james.darnley@gmail.com>
;*
;* This File is part of Advanced Visualization Studio.
;*
;* This program is free software: you can redistribute it and/or modify
;* it under the terms of the GNU General Public License as published by
;* the Free Software Foundation, either version 3 of the License, or
;* (at your option) any later version.
;*
;* This program is distributed in the hope that it will be useful,
;* but WITHOUT ANY WARRANTY; without even the implied warranty of
;* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;* GNU General Public License for more details.
;*
;* You should have received a copy of the GNU General Public License
;* along with this program.  If not, see <http://www.gnu.org/licenses/>.
;*

%include "x86/x86util.asm"

SECTION_RODATA

const pb_0
    times 32 db 0

const pb_127
    times 32 db 127

const pw_255
    times 16 dw 255
