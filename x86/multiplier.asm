;*
;* Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
;*
;* This File is part of Advanced Visualization Studio.
;*
;* This program is free software: you can redistribute it and/or modify
;* it under the terms of the GNU General Public License as published by
;* the Free Software Foundation, either version 3 of the License, or
;* (at your option) any later version.
;*
;* This program is distributed in the hope that it will be useful,
;* but WITHOUT ANY WARRANTY; without even the implied warranty of
;* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;* GNU General Public License for more details.
;*
;* You should have received a copy of the GNU General Public License
;* along with this program.  If not, see <http://www.gnu.org/licenses/>.
;*

%include "x86/x86util.asm"
%include "x86/constants.inc"

SECTION_RODATA

%define div_mask1 pb_127
div_mask2: times 32 db 0x3f
div_mask3: times 32 db 0x1f
pd_white:  times  8 dd 0xffffff

SECTION_TEXT

%macro emms_if_mmx 0
    %if mmsize == 8
        emms
    %endif
%endmacro

%macro MULTIPLIER_MUL 1
cglobal multiplier_mul%1, 2, 2, 2, 0, frame, pixels
    .loop:
        movu   m0,      [frameq]
        %rep %1
            paddusb m0, m0
        %endrep
        movu  [frameq],  m0

        add frameq,  mmsize
        sub pixelsd, mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

%macro MULTIPLIER_DIV 1
cglobal multiplier_div%1, 2, 2, 2, 0, frame, pixels
    mova      m1,  [div_mask%1]
    .loop:
        movu   m0,      [frameq]
        psrld  m0,       %1
        pand   m0,       m1
        movu  [frameq],  m0

        add frameq,  mmsize
        sub pixelsd, mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

%macro MULTIPLIER_INFRT 0
cglobal multiplier_infrt, 2, 2, 1, 0, frame, pixels
    .loop:
        movu     m0,      [frameq]
        pcmpgtd  m0,      [pb_0]
        movu    [frameq],  m0

        add frameq,  mmsize
        sub pixelsd, mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

%macro MULTIPLIER_INFSQ 0
cglobal multiplier_infsq, 2, 2, 1, 0, frame, pixels
    .loop:
        movu     m0,      [frameq]
        pcmpeqd  m0,      [pd_white]
        movu    [frameq],  m0

        add frameq,  mmsize
        sub pixelsd, mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
MULTIPLIER_MUL 1
MULTIPLIER_MUL 2
MULTIPLIER_MUL 3
MULTIPLIER_DIV 1
MULTIPLIER_DIV 2
MULTIPLIER_DIV 3
MULTIPLIER_INFRT
MULTIPLIER_INFSQ

INIT_XMM sse2
MULTIPLIER_MUL 1
MULTIPLIER_MUL 2
MULTIPLIER_MUL 3
MULTIPLIER_DIV 1
MULTIPLIER_DIV 2
MULTIPLIER_DIV 3
MULTIPLIER_INFRT
MULTIPLIER_INFSQ

INIT_YMM avx2
MULTIPLIER_MUL 1
MULTIPLIER_MUL 2
MULTIPLIER_MUL 3
MULTIPLIER_DIV 1
MULTIPLIER_DIV 2
MULTIPLIER_DIV 3
MULTIPLIER_INFRT
MULTIPLIER_INFSQ
