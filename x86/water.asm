;*
;* Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
;*
;* This File is part of Advanced Visualization Studio.
;*
;* This program is free software: you can redistribute it and/or modify
;* it under the terms of the GNU General Public License as published by
;* the Free Software Foundation, either version 3 of the License, or
;* (at your option) any later version.
;*
;* This program is distributed in the hope that it will be useful,
;* but WITHOUT ANY WARRANTY; without even the implied warranty of
;* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;* GNU General Public License for more details.
;*
;* You should have received a copy of the GNU General Public License
;* along with this program.  If not, see <http://www.gnu.org/licenses/>.
;*

%include "x86/x86util.asm"
%include "x86/constants.inc"

SECTION_TEXT

%macro emms_if_mmx 0
    %if mmsize == 8
        emms
    %endif
%endmacro

%macro WATER_LINE 0
cglobal water_line, 4, 5, 6, 0, output, input, buffer, w, x
    movsxdifnidn wq, wd

    pxor m0,     m0
    mov  xq,     wq
    sub  xq,     2
    shl  wq,     2
    sub  inputq, wq

    .loop:
        movh       m1,       [inputq+wq+4] ; frame[1]
        movh       m2,       [inputq+wq-4] ; frame[-1]
        movh       m3,       [inputq]      ; frame[-w]
        movh       m4,       [inputq+wq*2] ; frame[w]
        punpcklbw  m1,        m0
        punpcklbw  m2,        m0
        punpcklbw  m3,        m0
        punpcklbw  m4,        m0

        paddw      m1,        m2
        paddw      m3,        m4
        paddw      m1,        m3
        psrlw      m1,        1

        movh       m5,       [bufferq]
        punpcklbw  m5,        m0

        psubw      m1,        m5

        packuswb   m1,        m1
        movh      [outputq],  m1

        add        outputq,   mmsize/2
        add        inputq,    mmsize/2
        add        bufferq,   mmsize/2
        sub        xq,        mmsize/8
    jge .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
WATER_LINE

INIT_XMM sse2
WATER_LINE

; Using this correctly needs a little more work than I want to spend right now.
;INIT_YMM avx2
;WATER_LINE
