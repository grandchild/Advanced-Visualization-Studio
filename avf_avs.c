/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <avs.h>

#include "libavcodec/avfft.h"
#include "libavfilter/internal.h"
#include "libavutil/channel_layout.h"
#include "libavutil/opt.h"

#define TRANSFORM_BITS 10 /* 1024 samples */
#define VIS_DATA_BUFFERS 6
#define FREQ_DATA_BUFFERS 3

typedef struct {
    const AVClass *class;
    /* User options */
    char *file;
    int w, h;
    AVRational rate;
    /* Our variables */
    uint8_t *vis_data[VIS_DATA_BUFFERS];
    FFTSample *freq_data[FREQ_DATA_BUFFERS];
    int last_pts;
    float max_freq_value;
    /* Other contexts */
    AVSContext *avs_context;
    void *transform_context;
} JDContext;

#define OFFSET(x) offsetof(JDContext, x)
#define FLAGS AV_OPT_FLAG_FILTERING_PARAM|AV_OPT_FLAG_VIDEO_PARAM
static const AVOption jd_options[] = {
    { "file", "preset",     OFFSET(file), AV_OPT_TYPE_STRING,     {.str = NULL},  0, 0, FLAGS },
    { "size", "video size", OFFSET(w),    AV_OPT_TYPE_IMAGE_SIZE, {.str = "vga"}, 0, 0, FLAGS },
    { "s",    "video size", OFFSET(w),    AV_OPT_TYPE_IMAGE_SIZE, {.str = "vga"}, 0, 0, FLAGS },
    { "rate", "video rate", OFFSET(rate), AV_OPT_TYPE_VIDEO_RATE, {.str = "30"}, 0, 0, FLAGS },
    { "r",    "video rate", OFFSET(rate), AV_OPT_TYPE_VIDEO_RATE, {.str = "30"}, 0, 0, FLAGS },
    { NULL }
};

AVFILTER_DEFINE_CLASS(jd);

static int initialize(AVFilterContext *ctx)
{
    JDContext *jdctx = ctx->priv;
    int i, size = FFMAX(1<<TRANSFORM_BITS, 576);

    for (i = 0; i < FREQ_DATA_BUFFERS; i++) {
        jdctx->freq_data[i] = av_malloc_array(size, sizeof(FFTSample));
        if (!jdctx->freq_data[i])
            return AVERROR(ENOMEM);
    }

    for (i = 0; i < VIS_DATA_BUFFERS; i++) {
        jdctx->vis_data[i] = av_mallocz_array(576, sizeof(uint8_t));
        if (!jdctx->vis_data[i])
            return AVERROR(ENOMEM);
    }

    jdctx->transform_context = av_rdft_init(TRANSFORM_BITS, 0);
    if (!jdctx->transform_context)
        return AVERROR(ENOMEM);

    return avs_init(&jdctx->avs_context, jdctx->file, av_get_cpu_flags());
}

static void uninit(AVFilterContext *ctx)
{
    JDContext *jdctx = ctx->priv;
    int i;

    av_log(jdctx, AV_LOG_VERBOSE, "max_freq_value = %0.6f\n", jdctx->max_freq_value);

    for (i = 0; i < FREQ_DATA_BUFFERS; i++) {
        if (jdctx->freq_data[i])
            av_free(jdctx->freq_data[i]);
    }

    for (i = 0; i < VIS_DATA_BUFFERS; i++) {
        if (jdctx->vis_data[i])
            av_free(jdctx->vis_data[i]);
    }

    av_rdft_end(jdctx->transform_context);
    avs_uninit(jdctx->avs_context);

    return;
}

static int query_formats(AVFilterContext *ctx)
{
    AVFilterFormats *formats = NULL;
    AVFilterChannelLayouts *layouts = NULL;
    AVFilterLink *inlink = ctx->inputs[0];
    AVFilterLink *outlink = ctx->outputs[0];
    static const enum AVSampleFormat sample_fmts[] = { AV_SAMPLE_FMT_FLT, AV_SAMPLE_FMT_NONE };
    static const enum AVPixelFormat pixel_fmts[] = { AV_PIX_FMT_BGR0, AV_PIX_FMT_NONE };
    static const int64_t channel_layouts[] = { AV_CH_LAYOUT_MONO, -1 };

    formats = ff_make_format_list(sample_fmts);
    if (!formats)
        return AVERROR(ENOMEM);
    ff_formats_ref(formats, &inlink->out_formats);

    layouts = avfilter_make_format64_list(channel_layouts);
    if (!layouts)
        return AVERROR(ENOMEM);
    ff_channel_layouts_ref(layouts, &inlink->out_channel_layouts);

    formats = ff_all_samplerates();
    if (!formats)
        return AVERROR(ENOMEM);
    ff_formats_ref(formats, &inlink->out_samplerates);

    formats = ff_make_format_list(pixel_fmts);
    if (!formats)
        return AVERROR(ENOMEM);
    ff_formats_ref(formats, &outlink->in_formats);

    return 0;
}

static int configure_output_properties(AVFilterLink *outlink)
{
    JDContext *jdctx = outlink->src->priv;

    outlink->w = jdctx->w;
    outlink->h = jdctx->h;
    outlink->sample_aspect_ratio = (AVRational){1,1};
    outlink->time_base = av_inv_q(jdctx->rate);
    outlink->frame_rate = jdctx->rate;

    return 0;
}

static int configure_input_properties(AVFilterLink *inlink)
{
    JDContext *jdctx = inlink->dst->priv;

    int samples_per_frame = inlink->sample_rate / av_q2d(jdctx->rate) + 0.5;

    if (samples_per_frame < FFMAX(576, 1<<TRANSFORM_BITS)) {
        av_log(jdctx, AV_LOG_ERROR, "frame rate (%d/%d) too high for sample rate (%d)\n", jdctx->rate.num, jdctx->rate.den, inlink->sample_rate);
        return AVERROR(EINVAL);
    }

    inlink->min_samples = inlink->max_samples = inlink->partial_buf_size = samples_per_frame;

    return 0;
}

static int compare_pts(AVRational a_time_base, int64_t a_pts,
                       AVRational b_time_base, int64_t b_pts)
{
    double a_time = a_pts * av_q2d(a_time_base);
    double b_time = b_pts * av_q2d(b_time_base);

    if (a_time > b_time)
        return 1;
    if (a_time < b_time)
        return -1;
    return 0;
}

#define CLIP(a) ((a) < 0.0f ? 0.0f : (a) > 255.0f ? 255.0f : (a))

static void f32_to_u8(uint8_t out[576], float in[576])
{
    int i;
    for (i = 0; i < 576; i++) {
        out[i] = in[i] * 128.0f + 128.0f;
    }
    return;
}

static void f32_to_u8_freq(uint8_t out[576], FFTSample in[576], float *max)
{
    int i, i_max = FFMIN(576, 1 << (TRANSFORM_BITS - 1));
    float w = 1.0f / (sqrtf(1 << (TRANSFORM_BITS - 1)) * 32768.0f);

    for (i = 0; i < i_max; i+=1) {
        float a = hypotf(in[i*2], in[i*2+1]);
        if (a > *max) *max = a;
        a *= w;
        //a = sqrtf(a);
        out[i] = CLIP(a);
    }
    return;
}

static int filter_frame(AVFilterLink *inlink, AVFrame *in)
{
    AVFilterContext *ctx = inlink->dst;
    AVFilterLink *outlink = ctx->outputs[0];
    JDContext *jdctx = ctx->priv;
    AVFrame *out;
    int i, ret;
    uint8_t *frame;

    AVSDataContext avsdc = {};

    out = ff_get_video_buffer(outlink, jdctx->w, jdctx->h);
    if (!out)
        return AVERROR(ENOMEM);

    memcpy(jdctx->freq_data[0], in->data[0], (1<<TRANSFORM_BITS) * sizeof(FFTSample));
    av_rdft_calc(jdctx->transform_context, jdctx->freq_data[0]);
    f32_to_u8_freq(jdctx->vis_data[0], jdctx->freq_data[0], &jdctx->max_freq_value);
    f32_to_u8(jdctx->vis_data[0+3], (float *)in->data[0]);

    avsdc = (AVSDataContext){ .width = jdctx->w, .height = jdctx->h,
        .waveform = { jdctx->vis_data[3], jdctx->vis_data[3], jdctx->vis_data[3] },
        .spectrum = { jdctx->vis_data[0], jdctx->vis_data[0], jdctx->vis_data[0] },
    };

    //for (i = 0; i < jdctx->h; i++)
        //memset(out->data[0] + i * out->linesize[0], 0, jdctx->w * 4);

    ret = avs_render_frame(jdctx->avs_context, &avsdc,
            (jdctx->last_pts++) * ((1000.0 * jdctx->rate.den) / jdctx->rate.num));
    if (ret)
        return AVERROR_EOF;

    frame = avs_get_frame(jdctx->avs_context);

    for (i = 0; i < jdctx->h; i++) {
        memcpy(out->data[0] + i * out->linesize[0],
                frame + i * jdctx->w * sizeof(int),
                jdctx->w * sizeof(int));
    }

    out->pts = jdctx->last_pts;
    ret = ff_filter_frame(outlink, out);

    av_frame_free(&in);

    return ret;
}

static const AVFilterPad jd_inputs[] = {
    {
        .name = "default",
        .type = AVMEDIA_TYPE_AUDIO,
        .config_props = configure_input_properties,
        .filter_frame = filter_frame,
    },
    { NULL}
};
static const AVFilterPad jd_outputs[] = {
    {
        .name = "default",
        .type = AVMEDIA_TYPE_VIDEO,
        .config_props = configure_output_properties,
        //.request_frame = request_frame,
    },
    { NULL }
};

AVFilter ff_avf_avs = {
    .name        = "jd",
    .description = "Advanced Visualisation Studio.",
    .init        = initialize,
    .uninit      = uninit,
    .query_formats = query_formats,
    .inputs      = jd_inputs,
    .outputs     = jd_outputs,
    .priv_size   = sizeof(JDContext),
    .priv_class  = &jd_class,
};
