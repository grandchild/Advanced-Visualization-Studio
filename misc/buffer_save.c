/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "block.h"
#include "components.h"
#include "common.h"

enum {
    DIRECTION_SAVE,
    DIRECTION_RESTORE,
    DIRECTION_ALTERNATE,
};

typedef struct {
    int dir;
    int which;
    int blend;
    int adj_blend_value;
    int dir_ch;
} StackContext;

/* Blend modes:
 * -  0 = replace
 * -  1 = 50/50 or average
 * -  2 = additive
 * -  3 = every other pixel
 * -  4 = subtractive 1
 * -  5 = every other line
 * -  6 = xor
 * -  7 = maximum
 * -  8 = minimum
 * -  9 = subtractive 2
 * - 10 = multiply
 * - 11 = adjustable
 */

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    StackContext *stack = ctx->priv;
    int pos = 0;
    R32(stack->dir);
    R32(stack->which);
    R32(stack->blend);
    R32(stack->adj_blend_value);
    if (stack->which < 0)
        stack->which = 0;
    if (stack->which >= 8)
        stack->which = 8 - 1;
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    StackContext *stack = ctx->priv;
    int *buffer = ctx->actx->global_buffer[stack->which];
    int *fb_in, *fb_out;

    if (!buffer) {
        /* If no other component has yet allocated this buffer then return. */
        if (stack->dir == DIRECTION_RESTORE)
            return 0;

        /* Allocate it because we want to use it. */
        else {
            buffer = calloc(w * h, sizeof(int));
            if (!buffer)
                return -1;
            ctx->actx->global_buffer[stack->which] = buffer;
        }
    }

    int t_dir = stack->dir;
    if (t_dir == DIRECTION_ALTERNATE) {
        t_dir = stack->dir_ch;
        stack->dir_ch ^= 1;
    }

    if (t_dir == DIRECTION_RESTORE) {
        fb_in = buffer;
        fb_out = frame;
    } else {
        fb_in = frame;
        fb_out = buffer;
    }

    switch (stack->blend) {
        case 1:
            blend_block_avg(fb_out, fb_out, fb_in, w * h);
            break;
        case 2:
            blend_block_add(fb_out, fb_out, fb_in, w * h);
            break;
        case 3: {
            /* every other pixel */
            int r = 0;
            for (int y = h; y > 0; y--) {
                int *in = fb_in + r;
                int *out = fb_out + r;
                r ^= 1;
                for (int x = w/2; x > 0; x--) {
                    *out = *in;
                    out += 2;
                    in += 2;
                }
                fb_out += w;
                fb_in += w;
            }
            break; }
        case 4:
            blend_block_sub(fb_out, fb_out, fb_in, w * h);
            break;
        case 5:
            /* every other line */
            for (int y = h/2; y > 0; y--) {
                memcpy(fb_out, fb_in, w * sizeof(int));
                fb_out += w*2;
                fb_in  += w*2;
            }
            break;
        case 6:
            blend_block_xor(fb_out, fb_out, fb_in, w * h);
            break;
        case 7:
            blend_block_max(fb_out, fb_out, fb_in, w * h);
            break;
        case 8:
            blend_block_min(fb_out, fb_out, fb_in, w * h);
            break;
        case 9:
            blend_block_sub(fb_out, fb_in, fb_out, w * h);
            break;
        case 10:
            blend_block_mul(fb_out, fb_out, fb_in, w * h);
            break;
        case 11:
            blend_block_adj(fb_out, fb_in, fb_out, w * h, stack->adj_blend_value);
            break;
        default:
            memcpy(fb_out, fb_in, w * h * sizeof(int));
    }

    return 0;
}

Component m_stack = {
    .name = "Misc / Buffer Save",
    .code = 18,
    .priv_size = sizeof(StackContext),
    .load_config = load_config,
    .render = render,
};
