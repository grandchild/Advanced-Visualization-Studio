# TODO list

## 1 - Bugs

This is a list of all bugs in the new code that have been found and not fixed.

### render/superscope

* I believe there is an issue with emulating the mod (`%`) operator using Lua.
  The Lua docs seem to imply that the floating-point remainder will contin the
  decimal part.  Whereas the Winamp expression docs clearly state it converts
  both numbers to integers.  I need to investigate further.

### trans/interferences.c

* There seems to be some bug with it using its on-beat code.  I'm not sure what
  it is but it could be because status can never be greater than Pi.

### trans/roto_blit.c

* There seems to be a problem with this transforming pixels within a square in
  the center of the frame.  It is only visible with some of the blend options.
  I need to check which and document that here.

### x86/block.asm

* There is a bug in the multiply functions that I can't see.  I have left the
  assignment of the function commented out for now.

## 2 - Missing features

This is a list of features missing from already ported parts.

### SIMD (MMX, SSE)

* Some of the ported trans components have inline MMX blocks in the original AVS
  code.  This should be ported into external files.

### Threads

* Some of the components, e.g. blur and colour fade, seem to have basic support
  for sliced-based threading.  The original code only appears to use 1 slice.

* A short experiment with OpenMP showed that some speed-up is gained from
  threading.  However it wasn't quite as much as I expected.  I think that
  because of how I was using it, it created and destroyed threads everytime it
  entered its short lived block.  This makes me think that we want a threadpool:
  long lived threads that get given tasks.

### pixel.c:

* The `blend_pixel*()` functions are inline functions in the original AVS code.
  I am not against making these inline but I wonder if it is necessary.  The
  slowest part of most presets is probably the Superscope (and other
  expression-based components).

### trans/colour_map.c

* Cycling between maps is not implemented.  It needs: interpolation between the
  LUTs (which get filled at init), selecting Maps based on their enabled flag,
  not unconditionally using the first Map.

### trans/multi_filter.c

* The infinite square root mode has not yet been implemented.

### trans/texer.c:

* Needs to lookup a suitable texture based on what the preset requests.  That
  needs some thought and some Free textures.

* Replace input mode doesn't work yet, it needs to write the pixel to the output
  frame.  Simple enough I guess.

## 3 - Feature ideas

This is a list of ideas and thought about features to add.  Any features which
replace/fix/change original behaviour should be documented and should have an
option to disable them at compile time.

### Parser

* Rewrite the parser.  The current one probably isn't very safe from all sorts
  of problems.

* I planned on using Lua because that is already available. To do this a couple
  of utility functions need to be written first, namely reading binary values
  (floats, integers).  We could use Lua 5.3's string.unpack function but that
  would need backporting to the version we use.

### JSON input and output

* Presets should be able to be loaded from JSON files.  The javascript-based AVS
  File Decoder project has already established the format we should follow.

* Writing JSON files would also be a useful feature.  Combining it with a test
  of the preset would allow checking for correctness.

### Logging

* Currently the library just prints to stderr everywhere.  I don't know what
  this should look like.  Ideally it should work pretty seamlessly for all
  usersof the library.  FFmpeg should get the messages into av_log, vis_jdavs
  should log to a file and show errors to the user in a dialog box.

### Initialization

* Separate initialization from both preset parsing and rendering the first
  frame.

* A dedicated function member should be added to the Component struct.  Then
  components which need initialization or re-initialization should use this.

* When frame properties or options change this function in the root list should
  be called effecting the changes.

* Should there be a public trigger for this?

### Plugins for other software

* Got a favourite player you want to see AVS in?  A user on the Winamp forums
  already asked about a port for VLC.

### Options system

* Some options system is needed so that presets can be controlled by the user.
  It needs to toggle options, change values, change expressions.  Then the
  values need to get to the components.

* Option names should come from the AVS File Decoder project.  See this link for
  a list https://github.com/grandchild/AVS-File-Decoder-Qt/blob/master/components.json

### More docs

* Expand docs everywhere.  Document public and private functions.  Document
  structs.

* Speak to pak-9 and get his permission to use his AVS programming guide.  Use
  it to write a more up-to-date version and get it out of a Word Document file.
  It is available here http://pak-9.deviantart.com/art/AVS-Programming-Guide-11200891

### SIMD (MMX, SSE)

* External assembly code should get assembled by NASM and/or YASM.

* The x264asm abstraction layer should be used.

* It should all be optional at runtime.

* A compile-time option of a "minimum" required feature set is interesting.  It
  should disable C and any function variants that would never be called.

* A DSP context is needed for all the functions that already exist.  Currently
  they are just global variables.

### expressions.c:

* Document the original Nullsoft EEL code.

* Port the original Nullsoft EEL code.  It would be great to have it work on
  more than just x86 but it needs to at least compile with GCC.

* Improve the usage of the Lua API.  I'm sure some more speed can be eked out
  of it.

### trans/blur.c:

* Replace the blur with a better quality and/or a faster blur.

### trans/colour_map.c:

* Add a luma, or approximate luma key.  This might better represent percieved
  brightness than any of the existing options.  The approximation could be
  something like: `(2*red + 5*green + 1*blue)/8`.  This is near the existing
  ITU-R BT.{601,709,2020} and is simple integer math.

### trans/multiplier.c:

* Merge Fast Brightness into this component.  They do the same thing with
  largely the same code.
