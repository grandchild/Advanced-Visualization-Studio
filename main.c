/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "avs.h"
#include "blend.h"
#include "bpm.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

#if ARCH_X86
void init_x86(int cpu_flags);
#endif

extern const Component effect_list;

int avs_init(AVSContext **actx_out, AVSOptionsContext *options)
{
    int ret = 0;
    AVSContext *actx = 0;

    if (options->structure_size != sizeof(AVSOptionsContext)
            || options->api_version != AVS_API_VERSION) {
        ERROR("mismatching AVSOptionsContext ABI\n");
        ret = -1;
        goto error;
    }

    actx = calloc(1, sizeof(AVSContext));
    if (!actx) {
        ERROR("no memory\n");
        ret = -1;
        goto error;
    }
    actx->options = *options;
    avs_log_set_level(options->log_level);
    avs_log_set_callback(options->log_callback);
    actx->beat_context = init_bpm(BEAT_ADVANCED);
    if (!actx->beat_context) {
        ERROR("no memory for beat context\n");
        ret = -1;
        goto error;
    }

    actx->frame[0] = calloc(options->width * options->height, sizeof(int));
    actx->frame[1] = calloc(options->width * options->height, sizeof(int));
    if (!actx->frame[0] || !actx->frame[1]) {
        ERROR("no memory for root frames\n");
        ret = -1;
        goto error;
    }

    /* Initialize things. */
    register_components();
    init_blend_table();
#if ARCH_X86
    init_x86(options->cpu_flags);
#endif

error:
    *actx_out = actx;
    return ret;
}

static const char sig_0_1[] = "Nullsoft AVS Preset 0.1\x1a";
static const char sig_0_2[] = "Nullsoft AVS Preset 0.2\x1a";
static const int  sig_len   = sizeof(sig_0_2) - 1; /* Don't count the null char. */

static int read_file(const char *filename, uint8_t **out_buffer, int *out_size)
{
    int size, count;
    uint8_t *buffer = NULL;
    FILE *fh = NULL;

    fh = fopen((const char *)filename, "rb");
    if (!fh)
        goto error;

    if (fseek(fh, 0, SEEK_END) < 0)
        goto error;

    size = ftell(fh);
    if (size < 0 || size > 1048576) /* vis_avs uses a constant 1MiB buffer, */
        goto error;                 /* this makes for a useful sanity check */
                                    /* on the size. */

    if (fseek(fh, 0, SEEK_SET) < 0)
        goto error;

    buffer = malloc(size);
    if (!buffer)
        goto error;

    count = fread(buffer, 1, size, fh);
    if (count != size)
        goto error;

    *out_buffer = buffer;
    *out_size = size;

    fclose(fh);
    return 0;

error:
    *out_buffer = NULL;
    *out_size = 0;

    if (buffer)
        free(buffer);
    if (fh)
        fclose(fh);

    return -1;
}

int avs_load_preset(AVSContext *actx, const char *filename)
{
    int preset_size, ret;
    uint8_t *preset_buf = NULL;


    if (read_file(filename, &preset_buf, &preset_size)) {
        ERROR("Unable to read file: %s\n", filename);
        ret = -1;
        goto error;
    }

    if (preset_size < sig_len) {
        ERROR("preset file is too short\n");
        ret = -1;
        goto error;
    }

    /* Check that the signature string is present and correct.  There may be
     * some differences between the 0.1 and 0.2 versions so if a version 0.1
     * header is found, print a warning and ask for a report. */
    if (!memcmp(preset_buf, sig_0_1, sig_len)) {
        WARNING("AVS preset version 0.1 header detected.  Continuing but "
                "please report this to the Advanced Visualization Studio "
                "project\n");
    } else if (memcmp(preset_buf, sig_0_2, sig_len)) {
            ERROR("invalid preset header\n");
            ret = -1;
            goto error;
    }

    actx->root_list.priv = calloc(1, effect_list.priv_size);
    if (!actx->root_list.priv) {
        ret = -1;
        goto error;
    }

    actx->root_list.component = &effect_list;
    actx->root_list.blend_mode = blend_mode_default();
    actx->root_list.actx = actx;

    ret = effect_list.load_config(&actx->root_list, preset_buf + sig_len, preset_size - sig_len);

error:
    if (preset_buf)
        free(preset_buf);

    return ret;
}

void avs_print_preset(AVSContext *actx)
{
    if (!actx)
        return;

    effect_list.print_config(&actx->root_list, 0);

    return;
}

int avs_render_frame(AVSContext *actx, AVSDataContext *avsdc, uint32_t time)
{
    /* TODO: fill in missing data. */
    /* TODO: new frames for components that need it. */

    int beat = bpm_frame_has_beat(actx->beat_context, avsdc);
    int beat2 = refine_beat(actx->beat_context, beat, (time) ? time : 1);

    actx->avsdc = *avsdc;

    int ret = effect_list.render(&actx->root_list, NULL,
            actx->frame[0], actx->frame[1],
            actx->options.width, actx->options.height, beat2);
    if (ret < 0)
        return -1;

    if (ret == 1) {
        void *temp = actx->frame[0];
        actx->frame[0] = actx->frame[1];
        actx->frame[1] = temp;
    }

    return 0;
}

void *avs_get_frame(AVSContext *actx)
{
    return actx->frame[0];
}

void avs_uninit(AVSContext *actx)
{
    if (!actx)
        return;

    FREE(actx->beat_context);

    if (actx->root_list.priv) {
        effect_list.uninit(&actx->root_list);
        free(actx->root_list.priv);
    }

    for (int i = 0; i < lengthof(actx->global_buffer); i++)
        FREE(actx->global_buffer[i]);

    FREE(actx->frame[0]);
    FREE(actx->frame[1]);

    free(actx);
}
