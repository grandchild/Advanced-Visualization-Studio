/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>

#include "blend.h"
#include "block.h"
#include "pixel.h"

static void blend_block_avg_c(int *output, int *a, int *b, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_avg(*a, *b);
        output++;
        a++;
        b++;
    }
}

static void blend_block_add_c(int *output, int *a, int *b, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_add(*a, *b);
        output++;
        a++;
        b++;
    }
}

static void blend_block_sub_c(int *output, int *a, int *b, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_sub(*a, *b);
        output++;
        a++;
        b++;
    }
}

static void blend_block_xor_c(int *output, int *a, int *b, int pixels)
{
    while (pixels--) {
        *output = *a ^ *b;
        output++;
        a++;
        b++;
    }
}

static void blend_block_max_c(int *output, int *a, int *b, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_max(*a, *b);
        output++;
        a++;
        b++;
    }
}

static void blend_block_min_c(int *output, int *a, int *b, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_min(*a, *b);
        output++;
        a++;
        b++;
    }
}

static void blend_block_mul_c(int *output, int *a, int *b, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_mul(*a, *b);
        output++;
        a++;
        b++;
    }
}

static void blend_block_adj_c(int *output, int *a, int *b, int pixels, int v)
{
    while (pixels--) {
        *output = blend_pixel_adj(*a, *b, v);
        output++;
        a++;
        b++;
    }
}

void blend_block(int *output, int *a, int *b, int pixels, int blend_mode)
{
    switch (blend_mode_get_mode(blend_mode)) {
        case 1:
            blend_block_add(output, a, b, pixels);
            break;
        case 2:
            blend_block_max(output, a, b, pixels);
            break;
        case 3:
            blend_block_avg(output, a, b, pixels);
            break;
        case 4:
            blend_block_sub(output, a, b, pixels);
            break;
        case 5:
            blend_block_sub(output, b, a, pixels);
            break;
        case 6:
            blend_block_mul(output, a, b, pixels);
            break;
        case 7:
            blend_block_adj(output, a, b, pixels, blend_mode_get_adj(blend_mode));
            break;
        case 8:
            blend_block_xor(output, a, b, pixels);
            break;
        case 9:
            blend_block_min(output, a, b, pixels);
            break;
        /* Verbatim copy (case 0 and default) makes little sense here */
    }
}

void (*blend_block_avg)(int *, int *, int *, int) = blend_block_avg_c;
void (*blend_block_add)(int *, int *, int *, int) = blend_block_add_c;
void (*blend_block_sub)(int *, int *, int *, int) = blend_block_sub_c;
void (*blend_block_xor)(int *, int *, int *, int) = blend_block_xor_c;
void (*blend_block_max)(int *, int *, int *, int) = blend_block_max_c;
void (*blend_block_min)(int *, int *, int *, int) = blend_block_min_c;
void (*blend_block_mul)(int *, int *, int *, int) = blend_block_mul_c;
void (*blend_block_adj)(int *, int *, int *, int, int) = blend_block_adj_c;

static void blend_block_const_avg_c(int *output, int *a, int colour, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_avg(*a, colour);
        output++;
        a++;
    }
}

static void blend_block_const_add_c(int *output, int *a, int colour, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_add(*a, colour);
        output++;
        a++;
    }
}

static void blend_block_const_sub1_c(int *output, int *a, int colour, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_sub(*a, colour);
        output++;
        a++;
    }
}

static void blend_block_const_sub2_c(int *output, int colour, int *a, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_sub(colour, *a);
        output++;
        a++;
    }
}

static void blend_block_const_xor_c(int *output, int *a, int colour, int pixels)
{
    while (pixels--) {
        *output = *a ^ colour;
        output++;
        a++;
    }
}

static void blend_block_const_max_c(int *output, int *a, int colour, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_max(*a, colour);
        output++;
        a++;
    }
}

static void blend_block_const_min_c(int *output, int *a, int colour, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_min(*a, colour);
        output++;
        a++;
    }
}

static void blend_block_const_mul_c(int *output, int *a, int colour, int pixels)
{
    while (pixels--) {
        *output = blend_pixel_mul(*a, colour);
        output++;
        a++;
    }
}

static void blend_block_const_adj_c(int *output, int *a, int colour, int pixels, int v)
{
    while (pixels--) {
        *output = blend_pixel_adj(*a, colour, v);
        output++;
        a++;
    }
}

void blend_block_const(int *output, int *a, int colour, int pixels, int blend_mode)
{
    switch (blend_mode_get_mode(blend_mode)) {
        case 1:
            blend_block_const_add(output, a, colour, pixels);
            break;
        case 2:
            blend_block_const_max(output, a, colour, pixels);
            break;
        case 3:
            blend_block_const_avg(output, a, colour, pixels);
            break;
        case 4:
            blend_block_const_sub1(output, a, colour, pixels);
            break;
        case 5:
            blend_block_const_sub2(output, colour, a, pixels);
            break;
        case 6:
            blend_block_const_mul(output, a, colour, pixels);
            break;
        case 7:
            blend_block_const_adj(output, a, colour, pixels, blend_mode_get_adj(blend_mode));
            break;
        case 8:
            blend_block_const_xor(output, a, colour, pixels);
            break;
        case 9:
            blend_block_const_min(output, a, colour, pixels);
            break;
        default:
            while (pixels--)
                *output++ = colour;
    }
}

void (*blend_block_const_avg)(int *, int *, int, int) = blend_block_const_avg_c;
void (*blend_block_const_add)(int *, int *, int, int) = blend_block_const_add_c;
void (*blend_block_const_sub1)(int *, int *, int, int) = blend_block_const_sub1_c;
void (*blend_block_const_sub2)(int *, int, int *, int) = blend_block_const_sub2_c;
void (*blend_block_const_xor)(int *, int *, int, int) = blend_block_const_xor_c;
void (*blend_block_const_max)(int *, int *, int, int) = blend_block_const_max_c;
void (*blend_block_const_min)(int *, int *, int, int) = blend_block_const_min_c;
void (*blend_block_const_mul)(int *, int *, int, int) = blend_block_const_mul_c;
void (*blend_block_const_adj)(int *, int *, int, int, int) = blend_block_const_adj_c;
