/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

static int print_warning(ComponentContext *ctx, unused const uint8_t *buf, unused int buf_len)
{
    WARNING("This component (%s) is not implemented, continuing anyway.\n", ctx->component->name);
    return 0;
}

Component not_implemented_svp = {
    .name = "Render / SVP Loader",
    .code = 10,
    .load_config = print_warning,
};

Component not_implemented_text = {
    .name = "Render / Text",
    .code = 28,
    .load_config = print_warning,
};

Component not_implemented_avi = {
    .name = "Render / AVI",
    .code = 32,
    .load_config = print_warning,
};

Component not_implemented_bpm = {
    .name = "Misc / Custom BPM",
    .code = 33,
    .load_config = print_warning,
};

Component not_implemented_picture = {
    .name = "Render / Picture",
    .code = 34,
    .load_config = print_warning,
};

Component not_implemented_avstrans = {
    .name = "Misc / AVSTrans Automation",
    .signature = "Misc: AVSTrans Automation",
    .load_config = print_warning,
};

Component not_implemented_fpslimiter = {
    .name = "Misc / Framerate Limiter",
    .signature = "VFX FRAMERATE LIMITER",
    .load_config = print_warning,
};

Component not_implemented_convolution = {
    .name = "Trans / Convolution Filter",
    .signature = "Holden03: Convolution Filter",
    .load_config = print_warning,
};
