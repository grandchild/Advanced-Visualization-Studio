###############################################################################
# Recipes
###############################################################################

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $(CFLAGS_OPT) $(CFLAGS_DEP) -c -o $@ $<

%.o: %.asm
	$(DEPYASM) $(YASMFLAGS) -M -o $@ $< > $(@:.o=.d)
	$(YASM) $(YASMFLAGS) -o $@ $<
	$(STRIP) -wN '..@*' $@

%.a:
	$(AR) $(ARFLAGS) $@ $^
	$(RANLIB) $@

%_g$(EXESUF): %.o
	$(LD) $(LDFLAGS) -o $@ $< $(LIBRARY) $(LDLIBS)

%$(EXESUF): %_g$(EXESUF)
	$(CP) $< $@
	$(STRIP) $@
