/*
 * Copyright (c) 2016 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_LOG
#define AVS_HEADER_LOG 1

#include <stdarg.h>

/**
 * Format and print something to the log through the callback.
 */
void avs_log(int level, const char *fmt, ...);

/**
 * Set the log callback function.
 */
void avs_log_set_callback(void (*callback)(int, const char *, va_list));

/**
 * Set the log level.
 */
void avs_log_set_level(int level);

#define ERROR(message, ...) \
    avs_log(AVS_LOG_ERROR, "(%s:%d) [ERROR] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define WARNING(message, ...) \
    avs_log(AVS_LOG_WARNING, "(%s:%d) [WARNING] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define INFO(message, ...) \
    avs_log(AVS_LOG_INFO, "(%s:%d) [INFO] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define VERBOSE(message, ...) \
    avs_log(AVS_LOG_VERBOSE, "(%s:%d) [VERBOSE] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define DEBUG(message, ...) \
    avs_log(AVS_LOG_DEBUG, "(%s:%d) [DEBUG] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define CONT(message, ...) \
    avs_log(AVS_LOG_INFO, message, ##__VA_ARGS__)

#endif /* AVS_HEADER_LOG */
