#!python
# Copyright (c) 2016
#     James Darnley <james.darnley@gmail.com>,
#     Jakob K <grandchild@gmx.net>,
#     Sebastian Pipping <sebastian@pipping.org>
#
# This File is part of Advanced Visualization Studio.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import print_function
from platform import machine, system
from os.path import basename
from os import environ


'''
Config options
'''

flags_config = {
    'blend_pixel_accurate_avg': True,
    'blend_pixel_accurate_mul': False,
    'blend_pixel_inline': True,
    # 'debug_level': 1,
    'luajit': True,
    # 'openmp': False,
    # 'openmp_collapse': False,
    # 'opt_level': 3,
}


'''
Architecture and OS
'''

arch_list = {
    'x86': True,
    'x86_32': False,
    'x86_64': False,
}

have_list = {
    'aligned_stack': True,
    'cpunop': True,
}

system_list = {
    'cygwin': False,
    'linux': False,
    'windows': False,
}

flags_all = {'arch_': arch_list, 'config_': flags_config, 'have_': have_list, 'sys_': system_list}


'''
Configure
'''
# TODO: proper checks for platform to allow cross compiling.  This should be
# using what the tools report not what the operating system says.  For example:
# - A user might want to use ccache or clang.
# - aligned_stack is false on 32-bit msvc and icl.
# - cpunop is false with current nasm.

is_linux = system() == 'Linux'
is_cygwin = system().lower().startswith('cygwin')
is_windows = system() == 'Windows' or is_cygwin
is_mac = system() == 'MacOS'
is_unix = is_linux or is_mac
is_x86_64 = '64' in machine()

if is_x86_64:
    arch_list['x86_64'] = True
else:
    arch_list['x86_32'] = True

system_list['cygwin'] = is_cygwin
system_list['linux'] = is_unix
system_list['windows'] = is_windows

def get_config_header(flags_dicts, def_format):
    out = []
    for prefix, flags in flags_dicts.items():
        for k, v in flags.items():
            if isinstance(v, basestring):
                v = '"{}"'.format(v)
            else:
                try:
                    v = int(v)  # int() will coerce bools to '1' and '0' and raise an exception on anything else
                except:
                    print('Incompatible type for {} = {} ({})'.format(prefix+k, v, type(v)))
                    Exit(1)
            out.append(def_format.format(key=(prefix+k).upper(), val=v))
    return '\n'.join(out)

def write_asm_header(target, source, env):
    with open(str(target[0]), 'w') as f:
        print(get_config_header(flags_all, '%define {key} {val}'), file=f)

def write_c_header(target, source, env):
    with open(str(target[0]), 'w') as f:
        print('#ifndef AVS_HEADER_CONFIG\n#define AVS_HEADER_CONFIG 1\n', file=f)
        print(get_config_header(flags_all, '#define {key} {val}'), file=f)
        print('\n#endif /* AVS_HEADER_CONFIG */', file=f)


'''
Files
'''

src_lib = Split('''
        bpm.c
        block.c
        components.c
        effect_list.c
        expressions.c
        linedraw.c
        log.c
        main.c
        matrix.c
        not_implemented.c
        pixel.c
        utils.c
    ''')

src_misc = Glob('misc/*.c')
src_render = Glob('render/*.c')
src_trans = Glob('trans/*.c')
src_c = src_lib + src_misc + src_render + src_trans

src_x86_c = Split('''
        x86/init.c
''')
src_x86_asm = Split('''
        x86/constants.asm
        x86/block.asm
        x86/channel_shift.asm
        x86/colour_reduction.asm
        x86/fast_brightness.asm
        x86/multiplier.asm
        x86/unique_tone.asm
        x86/water.asm
    ''')
src_x86 = src_x86_c + src_x86_asm

src_winamp = Split('''
        vis_jdavs.c
    ''')


'''
Default Environment
'''

yasm_objfmt = '-f {}{}'.format('win' if system() == 'Windows' else 'elf', '64' if is_x86_64 else '32')
yasm_machine = '-m {}'.format('amd64' if is_x86_64 else 'x86')
yasm_debug = '' if is_windows else '-g dwarf2'
str_flags_config = ''.join([' -D{}={}'.format(key, value) for key, value in flags_config.iteritems()])

env = Environment(
        CC='gcc',  # if not is_cygwin else 'mingw32-gcc',
        CFLAGS='-pipe -Wall -Wextra -std=c99 -I. -I/usr/include/luajit-2.0 -include config.h',
        # CFLAGS_DEP='-MMD -MF $(@:.o=.d) -MT $@',
        LIBS=['m', 'luajit-5.1'],
        AS='yasm',
        ASFLAGS='-I. -P config.asm {f} {d} {m}'.format(
            f=yasm_objfmt,
            d=yasm_debug,
            m=yasm_machine),
        ENV={'PATH': environ['PATH']},
        CCCOMSTR='CC\t$TARGET',
        SHCCCOMSTR='CC\t$TARGET',
        ASCOMSTR='AS\t$TARGET',
        ARCOMSTR='AR\t$TARGET',
        LINKCOMSTR='LD\t$TARGET',
        SHLINKCOMSTR='LD\t$TARGET',
    )


'''
Custom builders & commands
'''

def createLuaHelper(target, source, env):
    with open(str(target[0]), 'w') as tf:
        for s in source:
            print('static const char *lua_{} ='.format(basename(str(s)).replace('-', '_').replace('.lua', '')), file=tf)
            with open(str(s), 'r') as sf:
                for line in sf:
                    print('"{}\\n"'.format(line.strip()), file=tf)
                print(';', file=tf)

lua_helper = env.Command(
        'lua-helper.h',
        Split('''
            doc/function-helper.lua
            doc/global-helper.lua
            doc/sanitize-helper.lua
        '''),
        createLuaHelper
    )
env.Depends('expressions.c', lua_helper)


def createX86Constants(target, source, env):
    with open(str(target[0]), 'w') as tf:
        with open(str(source[0]), 'r') as sf:
            for line in sf:
                if line.startswith('const'):
                    print(line.replace('const', 'cextern'), file=tf)

x86constants = env.Command(
        'x86/constants.inc',
        'x86/constants.asm',
        createX86Constants
    )


def strip_preproc_asm_lines(target, source, env):
    with open(str(target[0]), 'w') as tf:
        with open(str(source[0]), 'r') as sf:
            last_line_removed = True
            for line in sf:
                if (line.strip() == '' and last_line_removed) or line.strip().startswith('%'):
                    last_line_removed = True
                else:
                    print(line, file=tf)

env.Append(BUILDERS = {
        'StripLinesAsm': Builder(
                action=[strip_preproc_asm_lines, Delete('$SOURCE')],
                src_suffix='.dbg.asm_',
                suffix='.dbg.asm',
                single_source=True,
            )
    })

def print_generic_create(s, target, source, env):
    print('CREATE\t{}'.format(target[0]))


'''
Build Environments

*sh_* are shared library environments (needed for the assembler flag)
'''

env_opt = env.Clone()
env_opt.Append(CFLAGS=' -O3 -march=native -mmmx -msse -msse2 -ffast-math')
env_sh_opt = env_opt.Clone()
env_sh_opt.Append(ASFLAGS=' -DPIC')

env_dbg = env.Clone(DEBUG='1', OBJSUFFIX='.od')
env_dbg.Append(CFLAGS=' -O0 -g -march=native -mmmx -msse -msse2 -ffast-math')
env_sh_dbg = env_dbg.Clone(SHOBJSUFFIX='.osd')
env_sh_dbg.Append(ASFLAGS=' -DPIC')

env_dbg_test = env_dbg.Clone(LIBPATH='.')
env_dbg_test.Append(LIBS='avs_debug')
env_opt_test = env_opt.Clone(LIBPATH='.')
env_opt_test.Append(LIBS='avs')
# env_sh_test = env_sh_dbg.Clone(LIBPATH='.')
# env_sh_test.Append(LIBS=['avs', 'dl'], CFLAGS=' -std=gnu99')

env_winamp = env_sh_opt.Clone()
env_winamp.Append(CFLAGS=' -mwindows -mdll', LIBS=['gdi32', 'comdlg32'])

env_preproc = env.Clone(CCCOMSTR='PP\t$TARGET', ASCOMSTR='PP\t$TARGET')
env_preproc.Append(CFLAGS=' -E', ASFLAGS=' -e')


'''
Targets
'''

config_asm = env.Command('config.asm', None, write_asm_header, PRINT_CMD_LINE_FUNC=print_generic_create)
config_h = env.Command('config.h', None, write_c_header, PRINT_CMD_LINE_FUNC=print_generic_create)

obj_x86 = env_opt.StaticObject(src_x86)
obj_sh_x86 = env_sh_opt.SharedObject(src_x86)
obj_dbg_x86 = env_dbg.StaticObject(src_x86)
obj_sh_dbg_x86 = env_sh_dbg.SharedObject(src_x86)
env.Depends(obj_x86 + obj_sh_x86 + obj_dbg_x86 + obj_sh_dbg_x86, x86constants)

release_bin = [
        # opt.Program()
        env_opt.StaticLibrary('avs', src_c + obj_x86),
        env_sh_opt.SharedLibrary('avs', src_c + obj_sh_x86),
    ]

debug_bin = [
        # opt.Program()
        env_dbg.StaticLibrary('avs_debug', src_c + obj_dbg_x86),
        env_sh_dbg.SharedLibrary('avs_debug', src_c + obj_sh_dbg_x86),
    ]
tests_bin = [
        env_dbg_test.Program('tests/test.c'),
        env_opt_test.Program('tests/benchmark.c'),
        env_dbg_test.Program('tests/pixel.c'),
        # env_sh_test.Program('tests/dynamic.c'),
    ]

release = release_bin
debug = debug_bin + tests_bin
tests = tests_bin

winamp = env_winamp.SharedLibrary(src_winamp)
env.Depends(winamp, release_bin)
install_winamp = Install('/cygdrive/c/Winamp/Plugins/', winamp[0])

debug_preprocess_c = env_preproc.Object(src_c, OBJSUFFIX='.i')
debug_preprocess_asm = env_preproc.StripLinesAsm(
        env_preproc.Object(src_x86_asm, OBJSUFFIX='.dbg.asm_'),
        PRINT_CMD_LINE_FUNC=lambda *args: None  # just an empty function: print nothing
    )
env.Depends(debug_preprocess_asm + debug_preprocess_c, x86constants)

Alias('configure', config_asm + config_h)
Alias('preprocess', debug_preprocess_asm + debug_preprocess_c)
Alias('release', release_bin)
Alias('debug', debug_bin + tests_bin)
Alias('tests', tests_bin)
Alias('winamp', winamp)
Alias('install_winamp', install_winamp)


'''
Default targets (a.k.a. "What to Build")
'''

Default(release, debug)
if is_windows and not is_cygwin and not is_x86_64:
    Default(install_winamp)
